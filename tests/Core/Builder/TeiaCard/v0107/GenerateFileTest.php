<?php
namespace Netunna\Cnab\Tests\Template\TeiaCard\V0107;

use Carbon\Carbon;
use Netunna\Cnab\Common\TeiaCard\Adquirente;
use Netunna\Cnab\Common\TeiaCard\Empresa;
use Netunna\Cnab\Common\TeiaCard\Enum\AdquirenteEnum;
use Netunna\Cnab\Common\TeiaCard\Enum\BandeiraEnum;
use Netunna\Cnab\Common\TeiaCard\Enum\InscricaoEmpresaTipoEnum;
use Netunna\Cnab\Common\TeiaCard\Enum\MeioCapturaEnum;
use Netunna\Cnab\Common\TeiaCard\Enum\TipoServicoEnum;
use Netunna\Cnab\Common\TeiaCard\v0107\Lote;
use Netunna\Cnab\Common\TeiaCard\v0107\Remessa;
use Netunna\Cnab\Common\TeiaCard\v0107\SegmentoV;
use Netunna\Cnab\Core\Config;
use Netunna\Cnab\Core\ConfigTemplateEnum;

class GenerateFileTest extends \PHPUnit_Framework_TestCase {

	public function testGenerate() {
		$adquirente = new Adquirente();
		$adquirente
			->setNome( AdquirenteEnum::Stone() )
			->setNumeroEstabelecimento( '123' );

		$empresaSede = new Empresa();
		$empresaSede
			->setCodigo( '001' )
			->setNome( 'nome' )
			->setNumeroInscricao( '123' )
			->setTipoInscricao( new InscricaoEmpresaTipoEnum( InscricaoEmpresaTipoEnum::CgcCnpj ) );

		$empresaFilial = $empresaSede;

		$remessa = new Remessa();
		$remessa
//			->setNsa( Remessa::generateNsa() )
//			->setGeracaoArquivoDateTime( Carbon::now() )
			->setVersao( '01.07' )
			->setAdquirente( $adquirente )
			->setEmpresaSede( $empresaSede )
			->setEmpresaFilial( $empresaFilial );

		$lote = new Lote();
		$lote->setTipoServico( TipoServicoEnum::BaixasParcelaVendasCreditoAVista() );
		for ( $i = 0; $i < 2; $i++ ) {
			$segmento = new SegmentoV();
			$segmento
				->setNumeroCaixa( '000001' )
				->setNsu( '123' )
				->setBandeira( BandeiraEnum::Visa() )
				->setCodigoAutorizacao( '123' )
				->setDataVenda( Carbon::now() )
				->setValorBruto( 500 )
				->setTaxa( 1.2 )
				->setMeioCaptura( MeioCapturaEnum::ECommerce() )
				->setNumeroCartao( '123' )
				->setNumeroPedido( 33 )
				->setParcelas( 2 );

			$lote->addSegment( $segmento );
		}
		$remessa->addLote( $lote );

		$build = $remessa->build();

		$config = new Config();
		$config->load( ConfigTemplateEnum::TEIA_CARD_V0107 );
		$config->encodeFile( __DIR__ . '\test.txt', $build );
	}
}