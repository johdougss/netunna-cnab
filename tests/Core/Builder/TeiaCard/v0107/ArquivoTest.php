<?php

namespace Netunna\Cnab\Tests\Template\TeiaCard\V0107;

use Carbon\Carbon;
use Netunna\Cnab\Common\TeiaCard\Enum\RegistroEnum;
use Netunna\Cnab\Common\TeiaCard\Enum\TipoServicoEnum;
use Netunna\Cnab\Core\Builder\FileBuilder;
use Netunna\Cnab\Core\Builder\LineBuilder;
use Netunna\Cnab\Core\Builder\LotBuilder;
use Netunna\Cnab\Core\Builder\SegmentBuilder;
use Netunna\Cnab\Core\Config;
use Netunna\Cnab\Core\ConfigTemplateEnum;

class ArquivoTest extends \PHPUnit_Framework_TestCase {

	//<editor-fold desc="Encode">
	public function testEncode() {

		$fileBuilder = FileBuilder::create( [
			'header' => [
				'01.0' => 12,
			],
			'trailer' => [
				'01.9' => 123,
			],
			'lots' => [
				[
					'header' => [
						'01.1' => 333
					],
					'trailer' => [
						'01.3' => 333
					],
					'segments' => [
						'2V' => [
							[
								'01.2V' => 312,
								'02.2V' => 312,
								'03.2V' => 312,
							],
							[
								'01.2V' => 400,
								'02.2V' => 400,
								'03.2V' => 400,
							],
						],
						'2P' => [
							[
								'01.2P' => 312,
								'02.2P' => 3422,
							],
						],
					]
				],
			]
		] );

		$segmentoV = SegmentBuilder::create( [
			'01.2V' => 12,
			'02.2V' => 33,
			'06.2V' => 'V',
		] );
		$segmentoV->setTemplateId( '2V' );

		$segmentoP = SegmentBuilder::create( [
			'01.2P' => 12,
			'02.2P' => 44,
			'06.2P' => 'P'
		] );
		$segmentoP->setTemplateId( '2P' );

		$lote = new LotBuilder();
		$lote
			->setHeaderLot( [
				'01.1' => 12,
				'02.1' => 13,
			] )
			->setTrailerLot( [
				'01.3' => 12,
				'02.3' => 13,
			] )
			->addSegments( $segmentoV )
			->addSegments( $segmentoP );

		$fileBuilder->addLot( $lote );

		$expected = [
			'header' => [
				'01.0' => 12,
			],
			'trailer' => [
				'01.9' => 123,
			],
			'lots' => [
				[
					'header' => [
						'01.1' => 333
					],
					'trailer' => [
						'01.3' => 333
					],
					'segments' => [
						'2V' => [
							[
								'01.2V' => 312,
								'02.2V' => 312,
								'03.2V' => 312,
							],
							[
								'01.2V' => 400,
								'02.2V' => 400,
								'03.2V' => 400,
							],
						],
						'2P' => [
							[
								'01.2P' => 312,
								'02.2P' => 3422,
							],
						],
					]
				],
				[
					'header' => [
						'01.1' => 12,
						'02.1' => 13
					],
					'trailer' => [
						'01.3' => 12,
						'02.3' => 13
					],
					'segments' => [
						'2V' => [
							[
								'01.2V' => 12,
								'02.2V' => 33,
								'06.2V' => 'V',
							],
						],
						'2P' => [
							[
								'01.2P' => 12,
								'02.2P' => 44,
								'06.2P' => 'P'
							],
						]
					]
				],
			]
		];

		$this->assertEquals( $expected, $fileBuilder->toArray() );
	}

	public function testEncodeHeaderArquivo() {
		$config = new Config();

		$config->load( ConfigTemplateEnum::TEIA_CARD_V0107 );

		$strEncode = $config->getTemplateById( '0' )->encode( LineBuilder::create( [
			'01.0' => 111,
			'02.0' => null,
			'03.0' => RegistroEnum::HeaderArquivo,
			'04.0' => null,//'444444444',
			'05.0' => 5,
			'06.0' => '66666666666666',
			'07.0' => '7777777777777777777777777777777777777777',
			'08.0' => '8888888888888888888888888888888888888888',
			'09.0' => 9,
			'10.0' => 14011991,
			'11.0' => 111111,
			'12.0' => 2222222,
			'13.0' => '33333',
			'14.0' => '4444444444444444444444444444444444444444',
			'15.0' => '5555555555555555555555555555555555555555555555555555555555555',
		] ) );

		$this->assertEquals( 300, strlen( $strEncode ) );

		$this->assertEquals( '111', substr( $strEncode, 0, 3 ) );
		$this->assertEquals( '0000', substr( $strEncode, 3, 4 ) );
		$this->assertEquals( '0', substr( $strEncode, 7, 1 ) );
		$this->assertEquals( '         ', substr( $strEncode, 8, 9 ) );
		$this->assertEquals( '5', substr( $strEncode, 17, 1 ) );
		$this->assertEquals( '66666666666666', substr( $strEncode, 18, 14 ) );
		$this->assertEquals( '7777777777777777777777777777777777777777', substr( $strEncode, 32, 40 ) );
		$this->assertEquals( '8888888888888888888888888888888888888888', substr( $strEncode, 72, 40 ) );
		$this->assertEquals( '9', substr( $strEncode, 112, 1 ) );
		$this->assertEquals( '14011991', substr( $strEncode, 113, 8 ) );
		$this->assertEquals( '111111', substr( $strEncode, 121, 6 ) );
		$this->assertEquals( '2222222', substr( $strEncode, 127, 7 ) );
		$this->assertEquals( '33333', substr( $strEncode, 134, 5 ) );
		$this->assertEquals( '4444444444444444444444444444444444444444', substr( $strEncode, 139, 40 ) );
		$this->assertEquals( '5555555555555555555555555555555555555555555555555555555555555', substr( $strEncode, 179, 61 ) );
	}

	public function testEncodeTrailerArquivo() {
		$config = new Config();
		$config->load( ConfigTemplateEnum::TEIA_CARD_V0107 );

		$strEncode = $config->getTemplateById( '9' )->encode( LineBuilder::create( [
			'01.9' => 111,
			'02.9' => null,
			'03.9' => RegistroEnum::TrailerArquivo,
			'04.9' => '123',//'444444444',
			'05.9' => 555,
			'06.9' => '666666',
			'07.9' => null,
		] ) );

		$this->assertEquals( 300, strlen( $strEncode ) );

		$this->assertEquals( '111', substr( $strEncode, 0, 3 ) );
		$this->assertEquals( '9999', substr( $strEncode, 3, 4 ) );
		$this->assertEquals( '9', substr( $strEncode, 7, 1 ) );
		$this->assertEquals( '123      ', substr( $strEncode, 8, 9 ) );
		$this->assertEquals( '000555', substr( $strEncode, 17, 6 ) );
		$this->assertEquals( '666666', substr( $strEncode, 23, 6 ) );
		$this->assertEquals( str_pad( '', 211, ' ', STR_PAD_RIGHT ), substr( $strEncode, 29, 211 ) );
	}

//	public function testEncodeFile() {
//		$arquivoEncode = Arquivo::create( [
//			'header' => [
//				'01.0' => 111,
//				'05.0' => 5,
//			],
//			'trailer' => [
//				'01.9' => 123,
//			]
//		] );
//		$config = new Config();
//		$config->load( TemplateTypes::TEIA_CARD_V0107 );
////		$config->load(TemplateTypes::TEIA_CARD_V0107);
//
//		$filename = realpath( __DIR__ . '/../../../tests/fixtures/remessa/teia-card/test-encode-file.rem' );
//		$config->encodeFile( $filename, $arquivoEncode );
//
//		$content = file_get_contents( $filename );
//		$linhas = explode( "\n", $content );
//		$this->assertEquals( '111', substr( $linhas[ 0 ], 0, 3 ) );
//		$this->assertEquals( '5', substr( $linhas[ 0 ], 17, 1 ) );
//		$this->assertEquals( '123', substr( $linhas[ 1 ], 0, 3 ) );
//	}

	public function testEncodeSegmentoV() {
		$segmentoV = LineBuilder::create( [
			'01.2V' => null,
			'02.2V' => null,
			'03.2V' => null,
			'04.2V' => null,
			'05.2V' => null,
			'06.2V' => null,
			'07.2V' => null,
			'08.2V' => null,
			'09.2V' => null,
			'10.2V' => null,
			'11.2V' => null,
			'12.2V' => null,
			'13.2V' => null,
			'14.2V' => null,
			'15.2V' => null,
			'16.2V' => null,
			'17.2V' => null,
			'18.2V' => null,
			'19.2V' => null,
			'20.2V' => null,
			'21.2V' => null,
			'22.2V' => null,
			'23.2V' => null,
			'24.2V' => null,
			'25.2V' => null,
			'26.2V' => null,
			'27.2V' => null,
			'28.2V' => null,
			'29.2V' => null,
			'30.2V' => null,
		] );
		$config = new Config();
		$config->load( ConfigTemplateEnum::TEIA_CARD_V0107 );

		$actual = $config->getTemplateById( '2V' )->encode( $segmentoV );
		$expected = '000000020000000V 000000000000000                             0000000000000000000000000                                  000000000000000                                                                      000000000000000000000                                                                          ';

		$this->assertEquals( $expected, $actual );

	}
	//</editor-fold>

	//<editor-fold desc="Decode">
	public function testDecodeHeaderArquivoLine() {
		$str = "12500000         214317819000191DBR COMERCIO DE ARTIGOS DO VESTUARIO S.ACIELO                                   111072016071430000461101.05                                                                                                    ";
		$config = new Config();
		$config->load( ConfigTemplateEnum::TEIA_CARD_V0107 );

		$template = $config->getTemplateById( '0' );
		$actual = $template->decode( $str );
		$expected = [
			'01.0' => 125,
			'02.0' => 0,
			'03.0' => 0,
			'04.0' => '',
			'05.0' => 2,
			'06.0' => '14317819000191',
			'07.0' => 'DBR COMERCIO DE ARTIGOS DO VESTUARIO S.A',
			'08.0' => 'CIELO',
			'09.0' => 1,
			'10.0' => Carbon::create( 2016, 07, 11 ),
			'11.0' => Carbon::createFromTime( 7, 14, 30 ),
			'12.0' => 4611,
			'13.0' => '01.05',
			'14.0' => '',
			'15.0' => '',
		];
		$this->assertEquals( $expected, $actual );
	}

	public function testDecodeHeaderLoteLine() {
		$str = "125000110101     21431781900019112   1045769654                                                                                                                                                                                                 ";
		$config = new Config();
		$config->load( ConfigTemplateEnum::TEIA_CARD_V0107 );

		$template = $config->getTemplateById( '1' );
		$actual = $template->decode( $str );
		$expected = [
			'01.1' => 125,
			'02.1' => 1,
			'03.1' => 1,
			'04.1' => TipoServicoEnum::VendasCreditoAVista,
			'05.1' => '01',
			'06.1' => 2,
			'07.1' => 14317819000191,
			'08.1' => '12',
			'09.1' => '1045769654',
			'10.1' => '',
			'11.1' => '',
		];


		$this->assertEquals( $expected, $actual );
	}

	public function testDecodeTrailerLoteLine() {
		$str = "1250002302       21431781900019112   1045769654        0000120000000028273000000000276452                                                                                                                                                       ";
		$config = new Config();
		$config->load( ConfigTemplateEnum::TEIA_CARD_V0107 );

		$template = $config->getTemplateById( '3' );
		$actual = $template->decode( $str );
		$expected = [
			'01.3' => 125,
			'02.3' => 0002,
			'03.3' => 3,
			'04.3' => 2,
			'05.3' => '',
			'06.3' => 2,
			'07.3' => '14317819000191',
			'08.3' => 12,
			'09.3' => 1045769654,
			'10.3' => 12,
			'11.3' => 2827.30,
			'12.3' => 2764.52,
			'13.3' => '',
		];
		$this->assertEquals( $expected, $actual );
	}

	public function testDecodeTrailerArquivoLine() {
		$str = "12599999         000002000025                                                                                                                                                                                                                   ";
		$config = new Config();
		$config->load( ConfigTemplateEnum::TEIA_CARD_V0107 );

		$template = $config->getTemplateById( '9' );
		$actual = $template->decode( $str );
		$expected = [
			'01.9' => 125,
			'02.9' => 9999,
			'03.9' => 9,
			'04.9' => '',
			'05.9' => 2,
			'06.9' => 25,
			'07.9' => '',
		];
		$this->assertEquals( $expected, $actual );
	}

	public function testDecodeSegmentoVLine() {
		$str = "125000120100003V 21431781900019112   1045769654        0000010000000112036660097059304          455052    100720161431280000004289010050000000000056030000                                                   030000000000000000200650756                                                                    ";
		$config = new Config();
		$config->load( ConfigTemplateEnum::TEIA_CARD_V0107 );

		$template = $config->getTemplateById( '2V' );
		$actual = $template->decode( $str );
		$expected = [
			'01.2V' => 125,
			'02.2V' => 1,
			'03.2V' => 2,
			'04.2V' => 1,
			'05.2V' => 3,
			'06.2V' => 'V',
			'07.2V' => '',
			'08.2V' => 2,
			'09.2V' => '14317819000191',
			'10.2V' => '12',
			'11.2V' => '1045769654',
			'12.2V' => '000001',
			'13.2V' => 11203666,
			'14.2V' => 97059304,
			'15.2V' => '',
			'16.2V' => '455052',
			'17.2V' => Carbon::createFromDate( 2016, 07, 10 ),
			'18.2V' => Carbon::createFromTime( 14, 31, 28 ),
			'19.2V' => 42.89,
			'20.2V' => 1,
			'21.2V' => 5,
			'22.2V' => '0000000000056030000',
			'23.2V' => '',
			'24.2V' => '',
			'25.2V' => '',
			'26.2V' => 3,
			'27.2V' => 0,
			'28.2V' => 2.0,
			'29.2V' => '650756',
			'30.2V' => '',
		];
		$this->assertEquals( 300, strlen( $str ) );
		$this->assertEquals( $expected, $actual );
	}

//
//	public function testDecodeFile() {
//		$config = new Config();
//		$config->load( TemplateTypes::TEIA_CARD_V0107 );
//
//		$actual = $config->decodeFile( realpath( __DIR__ . '/../../../../tests/fixtures/remessa/teia-card/CDDBRVC16071105-2.rem' ) );
//		$expected = [
//			'header' => [
//				'01.0' => 125,
//				'02.0' => 0,
//				'03.0' => 0,
//				'04.0' => '',
//				'05.0' => 2,
//				'06.0' => '14317819000191',
//				'07.0' => 'DBR COMERCIO DE ARTIGOS DO VESTUARIO S.A',
//				'08.0' => 'CIELO',
//				'09.0' => 1,
//				'10.0' => Carbon::create( 2016, 07, 11 ),
//				'11.0' => Carbon::createFromTime( 7, 14, 30 ),
//				'12.0' => 4611,
//				'13.0' => '01.05',
//				'14.0' => '',
//				'15.0' => '',
//			],
//			'trailer' => [
//				'01.9' => 125,
//				'02.9' => 9999,
//				'03.9' => 9,
//				'04.9' => '',
//				'05.9' => 2,
//				'06.9' => 25,
//				'07.9' => '',
//			],
//			'lotes' => [
//				[
//					'header' => [
//						'01.1' => 125,
//						'02.1' => 1,
//						'03.1' => 1,
//						'04.1' => '1',
//						'05.1' => '',
//						'06.1' => 2,
//						'07.1' => '14317819000191',
//						'08.1' => '12',
//						'09.1' => '1045769654',
//						'10.1' => '',
//						'11.1' => '',
//					],
//					'trailer' => [
//						'01.3' => 125,
//						'02.3' => 1,
//						'03.3' => 3,
//						'04.3' => 1,
//						'05.3' => '',
//						'06.3' => 2,
//						'07.3' => '14317819000191',
//						'08.3' => 12,
//						'09.3' => 1045769654,
//						'10.3' => 7,
//						'11.3' => 328.29000000000002,
//						'12.3' => 321.70999999999998,
//						'13.3' => '',
//					],
//					'segmentos' => [
//						[
//							'tipo' => 'V',
//							'linha' => [
//								'01.2V' => 125,
//								'02.2V' => 2,
//								'03.2V' => 2,
//								'04.2V' => 2,
//								'05.2V' => 2,
//								'06.2V' => 'V',
//								'07.2V' => '',
//								'08.2V' => 2,
//								'09.2V' => '14317819000191',
//								'10.2V' => '12',
//								'11.2V' => '1045769654',
//								'12.2V' => '000001',
//								'13.2V' => 11203666,
//								'14.2V' => 97058267,
//								'15.2V' => 650742,
//								'16.2V' => '090845',
//								'17.2V' => Carbon::createFromDate( 2016, 07, 10 ),
//								'18.2V' => Carbon::createFromTime( 00, 15, 44 ),
//								'19.2V' => 152.55000000000001,
//								'20.2V' => 3,
//								'21.2V' => 6,
//								'22.2V' => '0000000000067300000',
//								'23.2V' => '',
//								'24.2V' => '',
//								'25.2V' => '',
//								'26.2V' => 3,
//								'27.2V' => 0,
//								'28.2V' => 2.2200000000000002,
//								'29.2V' => '',
//								'30.2V' => '',
//							]
//						],
//					],
//
//				],
//				[
//					'header' => [
//						'01.1' => 125,
//						'02.1' => 2,
//						'03.1' => 1,
//						'04.1' => 2,
//						'05.1' => '',
//						'06.1' => 2,
//						'07.1' => '14317819000191',
//						'08.1' => '12',
//						'09.1' => '1045769654',
//						'10.1' => '',
//						'11.1' => '',
//					],
//					'trailer' => [
//						'01.3' => 125,
//						'02.3' => 2,
//						'03.3' => 3,
//						'04.3' => 2,
//						'05.3' => '',
//						'06.3' => 2,
//						'07.3' => '14317819000191',
//						'08.3' => 12,
//						'09.3' => 1045769654,
//						'10.3' => 12,
//						'11.3' => 2827.3000000000002,
//						'12.3' => 2764.52,
//						'13.3' => '',
//					],
//					'segmentos' => [
//						[
//							'tipo' => 'V',
//							'linha' => [
//								'01.2V' => 125,
//								'02.2V' => 2,
//								'03.2V' => 2,
//								'04.2V' => 2,
//								'05.2V' => 2,
//								'06.2V' => 'V',
//								'07.2V' => '',
//								'08.2V' => 2,
//								'09.2V' => '14317819000191',
//								'10.2V' => '12',
//								'11.2V' => '1045769654',
//								'12.2V' => '000001',
//								'13.2V' => 11203666,
//								'14.2V' => 97058267,
//								'15.2V' => 650742,
//								'16.2V' => '090845',
//								'17.2V' => Carbon::createFromDate( 2016, 07, 10 ),
//								'18.2V' => Carbon::createFromTime( 00, 15, 44 ),
//								'19.2V' => 152.55000000000001,
//								'20.2V' => 3,
//								'21.2V' => 6,
//								'22.2V' => '0000000000067300000',
//								'23.2V' => '',
//								'24.2V' => '',
//								'25.2V' => '',
//								'26.2V' => 3,
//								'27.2V' => 0,
//								'28.2V' => 2.2200000000000002,
//								'29.2V' => '',
//								'30.2V' => '',
//							]
//						],
//						[
//							'tipo' => 'V',
//							'linha' => [
//								'01.2V' => 125,
//								'02.2V' => 2,
//								'03.2V' => 2,
//								'04.2V' => 2,
//								'05.2V' => 2,
//								'06.2V' => 'V',
//								'07.2V' => '',
//								'08.2V' => 2,
//								'09.2V' => '14317819000191',
//								'10.2V' => '12',
//								'11.2V' => '1045769654',
//								'12.2V' => '000001',
//								'13.2V' => 11203666,
//								'14.2V' => 97058267,
//								'15.2V' => 650742,
//								'16.2V' => '090845',
//								'17.2V' => Carbon::createFromDate( 2016, 07, 10 ),
//								'18.2V' => Carbon::createFromTime( 00, 15, 44 ),
//								'19.2V' => 152.55000000000001,
//								'20.2V' => 3,
//								'21.2V' => 6,
//								'22.2V' => '0000000000067300000',
//								'23.2V' => '',
//								'24.2V' => '',
//								'25.2V' => '',
//								'26.2V' => 3,
//								'27.2V' => 0,
//								'28.2V' => 2.2200000000000002,
//								'29.2V' => '',
//							]
//						],
//					],
//				]
//			]
//		];
//		$this->assertEquals( $expected, $actual->toArray() );
//	}
	//</editor-fold>

}