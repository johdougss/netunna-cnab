<?php

namespace Netunna\Cnab\Tests\Template\Core;

use Netunna\Cnab\Core\Picture\Picture;
use Netunna\Cnab\Core\Picture\PictureEnum;

class PictureTest extends \PHPUnit_Framework_TestCase {

	public function testFormatTipo() {
		$picture = new Picture();
		$picture->setFormat( 'x(9)' );
		$this->assertSame( PictureEnum::Alfanumerico, $picture->getFormatTipo() );
		$picture->setFormat( '9(9)' );
		$this->assertSame( PictureEnum::Numerico, $picture->getFormatTipo() );
	}

	public function formatFailProvider() {
		return [
			[ '9(9)AASDASD' ],
			[ '9(5' ],
			[ 'X' ],
			[ 'X(' ],
			[ 'X{' ],
			[ 'X(-10)' ],
			[ 'X()' ],
			[ 'X)' ],
		];
	}

	/**
	 * @dataProvider formatFailProvider
	 * @expectedException \Netunna\Cnab\Core\Exception\InvalidTemplateException
	 * @param $format
	 */
	public function testFormatInvalid( $format ) {
		$picture = new Picture();
		$this->expectExceptionMessage( "'$format' é um formato inválido" );
		$picture->setFormat( $format );
	}

	public function testFormatLength() {
		$picture = new Picture();
		$picture->setFormat( '9(5)' );
		$this->assertEquals( 5, $picture->getFormatLength() );
		$picture->setFormat( '9(5)V9(2)' );
		$this->assertEquals( 7, $picture->getFormatLength() );
	}
}
