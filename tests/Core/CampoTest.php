<?php

namespace Netunna\Cnab\Tests\Template\Core;

use Carbon\Carbon;
use Netunna\Cnab\Common\TeiaCard\Test;
use Netunna\Cnab\Core\Enum\MaskEnum;
use Netunna\Cnab\Core\Field;
use Netunna\Cnab\Core\Picture\Picture;

class CampoTest extends \PHPUnit_Framework_TestCase {
	public function formatDecodeProvider() {
		//$format, $decode, $expected
		return [
			//float number
			[ '9(3)V9(2)', '02042', 20.42 ],
			[ '9(3)V9(2)', '20000', 200.00 ],
			[ '9(3)V9(2)', '20005', 200.05 ],
			[ '9(3)V9(2)', '00250', 2.5 ],
			[ '9(2)V9(2)', '0333', 3.33 ],
			[ '9(3)V9(2)', '07046', 70.46 ],
			[ '9(8)V9(2)', '0000004289', 42.89 ],

			// integer number
			[ '9(5)', '02002', 2002 ],
			[ '9(1)', '3', 3, ],
			[ '9(10)', '2147483646', 2147483646 ], //converte para int
			[ '9(10)', '2147483647', '2147483647' ], //retorna como string
			[ '9(14)', '00000000000001', 1 ],
			[ '9(14)', '04317819000191', '4317819000191' ],
			[ '9(14)', '14317819000191', '14317819000191' ], //maior que o max int , //retorna como string

			// alphanumeric
			[ 'X(5)', '  Abc', 'Abc', ],
			[ 'X(5)', 'Abc  ', 'Abc', ],
			[ 'X(9)', '         ', '' ],


			//defaults
			[ '9(1)', null, 5, '5' ],
			[ 'X(5)', null, '', '     ' ],
		];
	}

	/**
	 * @dataProvider formatDecodeProvider
	 * @param      $format
	 * @param      $decode
	 * @param      $expected
	 * @param null $default
	 * @throws \Netunna\Cnab\Core\Exception\InvalidTemplateException
	 */
	public function testDecode( $format, $decode, $expected, $default = null ) {
		$campo = new Field();
		$campo->setDefault( $default );
		$campo->setPicture( new Picture( $format ) );
		$actual = $campo->decode( $decode );
		$this->assertSame( $expected, $actual );
	}

	public function testDecodeDate() {
		$campo = new Field();
		$campo->setMask( new MaskEnum( MaskEnum::Date ) );
		$campo->setPattern( 'dmY' );

		$campo->setPicture( new Picture( '9(8)' ) );
		/** @var Carbon $actual */
		$actual = $campo->decode( '14011991' );
		$this->assertInstanceOf( Carbon::class, $actual );
		$expected = Carbon::create( 1991, 01, 14 );
		$this->assertEquals( $expected->age, $actual->age );
		$this->assertEquals( $expected->month, $actual->month );
		$this->assertEquals( $expected->day, $actual->day );
	}

	public function testDecodeDateShort() {
		$campo = new Field();
		$campo->setMask( new MaskEnum( MaskEnum::Date ) );
		$campo->setPattern( 'dmy' );
		$campo->setPicture( new Picture( '9(8)' ) );
		$actual = $campo->decode( '140191' );
		$this->assertInstanceOf( Carbon::class, $actual );
		$this->assertEquals( Carbon::create( 1991, 01, 14 ), $actual );
	}


	public function formatEncodeProvider() {
		//$format, $encode, $expected, $default
		return [
			//float number
			[ '9(5)', 200.00, '00200' ],
			[ '9(3)V9(2)', 200.00, '20000' ],
			[ '9(3)V9(2)', 200.05, '20005' ],
			[ '9(3)V9(2)', 200.50, '20050' ],
			[ '9(3)V9(2)', 2.5, '00250' ],
			[ '9(3)V9(2)', 2.05, '00205' ],
			[ '9(3)V9(2)', 70.45999999999999, '07046' ],
			[ '9(3)V9(2)', 70.45111111111111, '07045' ],
			[ '9(3)V9(3)', 70.45111111111111, '070451' ],
			[ '9(3)V9(2)', 70.4591111111111, '07045' ],

			// integer number
			[ '9(5)', 200, '00200' ],
			[ '9(1)', 4, '4' ],

			// alphanumeric
			[ 'X(5)', 'abc', 'abc  ' ],
			[ 'x(5)', 'abc', 'abc  ' ],
			[ 'X(9)', '', '         ' ],

			//defaults
			[ 'X(2)', null, '  ' ],
			[ '9(3)', null, '000' ],
			[ '9(3)', null, '001', '1' ],
		];
	}

	/**
	 * @dataProvider formatEncodeProvider
	 * @param      $format
	 * @param      $encode
	 * @param      $expected
	 * @param null $default
	 * @throws \Exception
	 */
	public function testEncode( $format, $encode, $expected, $default = null ) {
		$campo = new Field();
		$campo->setDefault( $default );
		$campo->setPicture( new Picture( $format ) );
		$actual = $campo->encode( $encode );
		$this->assertSame( $expected, $actual );
	}

	public function testEncodeDate() {
		$campo = new Field();
		$campo->setMask( new MaskEnum( MaskEnum::Date ) );
		$campo->setPattern( 'dmY' );

		$campo->setPicture( new Picture( '9(8)' ) );

		$actual = $campo->encode( '14011991' );
		$this->assertSame( '14011991', $actual );

		$actual = $campo->encode( Carbon::create( 1991, 02, 22 ) );
		$this->assertSame( '22021991', $actual );

		$actual = $campo->encode( \DateTime::createFromFormat( 'mdY', '10302005' ) );
		$this->assertSame( '30102005', $actual );
	}

	public function testEncodeTime() {
		$campo = new Field();
		$campo->setMask( new MaskEnum( MaskEnum::Time ) );
		$campo->setPattern( 'His' );
		$campo->setPicture( new Picture( '9(6)' ) );

		$actual = $campo->encode( '143059' );
		$this->assertSame( '143059', $actual );

		$actual = $campo->encode( Carbon::create( 1991, 02, 22, 20, 5, 13 ) );
		$this->assertSame( '200513', $actual );

		$actual = $campo->encode( \DateTime::createFromFormat( 'mdYHis', '10302005235958' ) );
		$this->assertSame( '235958', $actual );
	}

	public function formatEncodeInvalidProvider() {
		//$format, $value,
		return [
//			//float number
			[ '9(1)V9(2)', 12.42 ],

			// integer number
			[ '9(1)', 12 ],

			// alphanumeric
			[ 'X(1)', 'ab' ],
			[ 'X(2)', 'abcd', 'ab' ],
		];
	}

	/**
	 * @dataProvider formatEncodeInvalidProvider
	 * @expectedException \Exception
	 * @param $format
	 * @param $value
	 * @throws \Exception
	 */
	public function testEncodeInvalid( $format, $value ) {
		$campo = new Field();
		$picture = new Picture( $format );
		$campo->setPicture( $picture );
		$campo->encode( $value );
		$this->expectExceptionMessage( "'$value' (" . strlen( $value ) . ") excedeu o tamanho máximo para essa picture (" . $picture->getFormatLength() . ")" );
	}


	/**
	 * @expectedException \Exception
	 * @expectedExceptionMessage O formato solicitado '9(5)', precisa que seu valor 'abc' seja do tipo numérico
	 * @throws \Exception
	 */
	public function testEncodeFormatNumberParamInvalid() {
		$campo = new Field();
		$picture = new Picture( '9(5)' );
		$campo->setPicture( $picture );
		$campo->encode( 'abc' );
	}
}
