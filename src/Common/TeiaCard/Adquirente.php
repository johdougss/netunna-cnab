<?php
namespace Netunna\Cnab\Common\TeiaCard;

use Netunna\Cnab\Common\TeiaCard\Enum\AdquirenteEnum;

class Adquirente {
	/** @var  AdquirenteEnum */
	protected $nome;

	/**
	 * Numero do estabelecimento da Empresa
	 */
	/** @var  string */
	protected $numeroEstabelecimento;

	//<editor-fold desc="Getters and Setters">
	/**
	 * @return AdquirenteEnum
	 */
	public function getNome() {
		return $this->nome;
	}

	/**
	 * @param AdquirenteEnum $nome
	 * @return $this
	 */
	public function setNome( $nome ) {
		$this->nome = $nome;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getNumeroEstabelecimento() {
		return $this->numeroEstabelecimento;
	}

	/**
	 * @param string $numeroEstabelecimento
	 * @return $this
	 */
	public function setNumeroEstabelecimento( $numeroEstabelecimento ) {
		$this->numeroEstabelecimento = $numeroEstabelecimento;
		return $this;
	}
	//</editor-fold>
}
