<?php
namespace Netunna\Cnab\Common\TeiaCard\Exception;

class BandeiraEnumException extends \Exception {
	public function __construct( $value ) {
		$message = 'A bandeira ' . $value . ' não pode ser convertida';
		parent::__construct( $message );
	}
}