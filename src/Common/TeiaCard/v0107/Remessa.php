<?php
namespace Netunna\Cnab\Common\TeiaCard\v0107;

use Carbon\Carbon;
use Netunna\Cnab\Common\TeiaCard\Adquirente;
use Netunna\Cnab\Common\TeiaCard\Empresa;
use Netunna\Cnab\Common\TeiaCard\Enum\ArquivoEnum;
use Netunna\Cnab\Common\TeiaCard\Enum\RegistroEnum;
use Netunna\Cnab\Core\Builder\FileBuilder;
use Netunna\Cnab\Core\Builder\LotBuilder as LoteBuild;
use Netunna\Cnab\Core\Builder\SegmentVBuilder as SegmentoVBuild;

class Remessa {

	/**
	 * @var string
	 */
	protected $versao;

	/**
	 * @var Empresa
	 */
	protected $empresaSede;
	/**
	 * @var Empresa
	 */
	protected $empresaFilial;

	/**
	 * @var Adquirente
	 */
	protected $adquirente;

	/**
	 * Numero sequencial
	 *
	 * @var
	 */
	protected $nsa;

	/**
	 * @var Carbon
	 */
	protected $geracaoArquivoDateTime;

	/**
	 * @var Lote[]
	 */
	protected $lotes;

	/**
	 * @return FileBuilder
	 */
	public function build() {
		$arquivo = new FileBuilder();

		if ( $this->nsa == null )
			$this->nsa = self::generateNsa();

		if ( $this->geracaoArquivoDateTime == null ) {
			$this->geracaoArquivoDateTime = Carbon::now();
		}

		$countLote = 0;
		$countLinhas = 0;

		$countLinhas++;
		$arquivo->setFileHeader( [
			'01.0' => $this->empresaSede->getCodigo(),
			'02.0' => null, //default
			'03.0' => RegistroEnum::HeaderArquivo,
			'04.0' => null, //teia-card
			'05.0' => $this->empresaSede->getTipoInscricao()->getValue(),
			'06.0' => $this->empresaSede->getNumeroInscricao(),
			'07.0' => $this->empresaSede->getNome(),
			'08.0' => $this->adquirente->getNome()->getValue(),
			'09.0' => ArquivoEnum::Remessa,
			'10.0' => $this->geracaoArquivoDateTime, //$now,
			'11.0' => $this->geracaoArquivoDateTime,
			'12.0' => $this->nsa,//NSA
			'13.0' => $this->versao,
			'14.0' => null, //uso reservado para empresa
			'15.0' => null, //teia-card
		] );

		foreach ( $this->lotes as $lote ) {
			$loteLayout = new LoteBuild();

			$countLote++;
			$countLinhas++;
			$loteLayout->setHeaderLot( [
				'01.1' => $this->empresaSede->getCodigo(),
				'02.1' => $countLote,
				'03.1' => RegistroEnum::HeaderLote,
				'04.1' => $lote->getTipoServico()->getValue(), //quando for 1 parcela é credito a vista, mas de 1 credito parcelado// perguntar como identifica se é debito ou credito --< os lotos sao difenciados pelo tipo de servico
				'05.1' => null, //teia-card
				'06.1' => $this->empresaFilial->getTipoInscricao()->getValue(),
				'07.1' => $this->empresaFilial->getNumeroInscricao(),
				'08.1' => $this->empresaFilial->getCodigo(),
				'09.1' => $this->adquirente->getNumeroEstabelecimento(),
				'10.1' => null, //uso reservado para empresa
				'11.1' => null, //teia-card
			] );


			$valorBrutoSegmentoTotal = 0;
			$valorLiquidoSegmentoTotal = 0;

			$countSegmento = 0;
			foreach ( $lote->getSegmentos() as $segmento ) {

				if ( $segmento instanceof SegmentoV ) {
					$countSegmento++;
					$segmentoVBuilder = SegmentoVBuild::create( [
						'01.2V' => $this->empresaSede->getCodigo(),
						'02.2V' => $countLote,
						'03.2V' => RegistroEnum::Segmento,
						'04.2V' => $lote->getTipoServico()->getValue(),
						'05.2V' => $countSegmento,
						'06.2V' => null,
						'07.2V' => null, //teia-card
						'08.2V' => $this->empresaFilial->getTipoInscricao()->getValue(),
						'09.2V' => $this->empresaFilial->getNumeroInscricao(),
						'10.2V' => $this->empresaFilial->getCodigo(),
						'11.2V' => $this->adquirente->getNumeroEstabelecimento(),
						'12.2V' => $segmento->getNumeroCaixa(), //quando for e-commerce
						'13.2V' => $segmento->getNumeroPdvMaquineta(), //número PDV(maquineta) adquirente ?? //Dependencia do Leandro - qual o numero da maquineta - Se tiver ..
						'14.2V' => $segmento->getNumeroPedido(), // $order->getId(),//COO: 715709 //número cupom fiscal/número pedido' ??
						'15.2V' => $segmento->getCupomFiscal(),//,//$transaction->getN //Cupom <---- Cupom de venda/ Cupom Fiscal(na frente do caixa)  - Preciso do comprovante de venda( Adquirente devolve quando uma transação é realizada ). //'número (NSU)(Numero da Arquirente para identificar a venda)(CV) ou (DOC)' ?? vem da Adquirente // Perguntar para BSeller onde esta esse comprovante de venda
						'16.2V' => $segmento->getCodigoAutorizacao(),//'código autorização' (MundiPag, codigo de autorizacao da venda)  ??  vem da Adquirente // Perguntar para BSeller
						'17.2V' => $segmento->getDataVenda(), //data da venda
						'18.2V' => $segmento->getDataVenda(), //hora da venda
						'19.2V' => $segmento->getValorBruto(),
						'20.2V' => $segmento->getParcelas(),
						'21.2V' => $segmento->getBandeira()->getValue(),
						'22.2V' => $segmento->getNumeroCartao(),
						'23.2V' => $segmento->getNomeProprietario(),//nome proprietario cartão
						'24.2V' => $segmento->getNomeOperadorCaixa(),//'nome operador caixa'
						'25.2V' => $segmento->getProgramaPromocional(),//'indicador programa promocional'
						'26.2V' => $segmento->getMeioCaptura()->getValue(),
						'27.2V' => $segmento->getNumeroPosMaquineta(), //número POS(Maquineta) Adquirente(Manual) ???
						'28.2V' => $segmento->getTaxa(), //$taxaComissao, //taxa comissão?? // Perguntar Onde esta a taxa de comissao cobrada pela adquirente. Se tiver.
						'29.2V' => $segmento->getNsu(),
						'30.2V' => null, //teia-card
					] );
					$valorBrutoSegmentoTotal += $segmento->getValorBruto();

					if ( $segmento->getTaxa() > 0 ) {
						$valorLiquidoSegmentoTotal += ( $segmento->getValorBruto() - ( $segmento->getValorBruto() * ( $segmento->getTaxa() / 100 ) ) );
					} else {
						$valorLiquidoSegmentoTotal += $segmento->getValorBruto();
					}

					$countLinhas++;
					$loteLayout->addSegments( $segmentoVBuilder );
				}
			}


			$loteLayout->setTrailerLot( [
				'01.3' => $this->empresaSede->getCodigo(),
				'02.3' => $countLote,
				'03.3' => RegistroEnum::TrailerLote,
				'04.3' => $lote->getTipoServico()->getValue(),
				'05.3' => null, //teia-card
				'06.3' => $this->empresaFilial->getTipoInscricao()->getValue(),
				'07.3' => $this->empresaFilial->getNumeroInscricao(),
				'08.3' => $this->empresaFilial->getCodigo(),
				'09.3' => $this->adquirente->getNumeroEstabelecimento(),
				'10.3' => $countSegmento, //quantidade total de segmentos V do lote
				'11.3' => $valorBrutoSegmentoTotal,
				'12.3' => $valorLiquidoSegmentoTotal, //por enquanto deixar 0 //$valorLiquidoTotal, //valor bruto - taxa(comissao)  // BSeller = perguntar se tem o valor liquido da venda.  valor bruto(50,00) taxa(10%) = 45,00
				'13.3' => null, //teia-card
			] );

			$countLinhas++;
			$arquivo->addLot( $loteLayout );
		}

		$countLinhas++;
		$arquivo->setFileTrailer( [
			'01.9' => $this->empresaSede->getCodigo(),
			'02.9' => null, //default
			'03.9' => RegistroEnum::TrailerArquivo,
			'04.9' => null, //teia-card
			'05.9' => $countLote,
			'06.9' => $countLinhas, //quantidade de linhas ou registros (conta tudo)
			'07.9' => null, //teia-card
		] );

		return $arquivo;
	}

	/**
	 * @return Lote[]
	 */
	public function getLotes() {
		return $this->lotes;
	}

	/**
	 * @param Lote[] $lotes
	 * @return $this
	 */
	public function setLotes( $lotes ) {
		$this->lotes = $lotes;
		return $this;
	}

	/**
	 * @param Lote $lote
	 * @return $this
	 */
	public function addLote( Lote $lote ) {
		if ( $this->lotes == null )
			$this->lotes = [ ];
		$this->lotes[] = $lote;
		return $this;
	}

	/**
	 * @param Carbon $dateStart
	 * @param Carbon $dateActual
	 * @return int
	 */
	public static function generateNsa( $dateStart = null, $dateActual = null ) {
		if ( $dateStart == null )
			$dateStart = Carbon::createFromDate( 2016, 2, 03 );
		if ( $dateActual == null )
			$dateActual = Carbon::now();
		return $dateStart->diff( $dateActual )->days + 1;
	}

	/**
	 * @return Empresa
	 */
	public function getEmpresaSede() {
		return $this->empresaSede;
	}

	/**
	 * @param Empresa $empresaSede
	 * @return $this
	 */
	public function setEmpresaSede( $empresaSede ) {
		$this->empresaSede = $empresaSede;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getVersao() {
		return $this->versao;
	}

	/**
	 * @param string $versao
	 * @return $this
	 */
	public function setVersao( $versao ) {
		$this->versao = $versao;
		return $this;
	}

	/**
	 * @return Adquirente
	 */
	public function getAdquirente() {
		return $this->adquirente;
	}

	/**
	 * @param Adquirente $adquirente
	 * @return $this
	 */
	public function setAdquirente( $adquirente ) {
		$this->adquirente = $adquirente;
		return $this;
	}

	/**
	 * @return Carbon
	 */
	public function getGeracaoArquivoDateTime() {
		return $this->geracaoArquivoDateTime;
	}

	/**
	 * @param Carbon $geracaoArquivoDateTime
	 * @return $this
	 */
	public function setGeracaoArquivoDateTime( $geracaoArquivoDateTime ) {
		$this->geracaoArquivoDateTime = $geracaoArquivoDateTime;
		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getNsa() {
		return $this->nsa;
	}

	/**
	 * @param mixed $nsa
	 * @return $this
	 */
	public function setNsa( $nsa ) {
		$this->nsa = $nsa;
		return $this;
	}

	/**
	 * @return Empresa
	 */
	public function getEmpresaFilial() {
		return $this->empresaFilial;
	}

	/**
	 * @param Empresa $empresaFilial
	 * @return $this
	 */
	public function setEmpresaFilial( $empresaFilial ) {
		$this->empresaFilial = $empresaFilial;
		return $this;
	}


}