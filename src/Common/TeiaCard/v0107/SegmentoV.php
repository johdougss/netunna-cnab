<?php
namespace Netunna\Cnab\Common\TeiaCard\v0107;

use Carbon\Carbon;
use Netunna\Cnab\Common\TeiaCard\Enum\BandeiraEnum;
use Netunna\Cnab\Common\TeiaCard\Enum\MeioCapturaEnum;

class SegmentoV extends Segmento {
	/**
	 * @var MeioCapturaEnum
	 */
	protected $meioCaptura;

	/**
	 * @var
	 */
	protected $nsu;

	/**
	 * @var
	 */
	protected $numeroCartao;

	/**
	 * @var BandeiraEnum
	 */
	protected $bandeira;

	/**
	 * @var int
	 */
	protected $parcelas;

	/**
	 * número cupom fiscal/número pedido
	 *
	 * @var string
	 */
	protected $numeroPedido;

	/**
	 * @var string
	 */
	protected $codigoAutorizacao;

	/**
	 * @var Carbon
	 */
	protected $dataVenda;

	/**
	 * @var float
	 */
	protected $valorBruto;

	/**
	 * Valor de 0 a 100%
	 *
	 * @var float
	 */
	protected $taxa;

	/**
	 * @var string
	 */
	protected $nomeProprietario;

	/**
	 * @var string
	 */
	protected $nomeOperadorCaixa;
	/**
	 * @var string
	 */
	protected $programaPromocional;

	/**
	 * @var string
	 */
	protected $numeroPdvMaquineta;

	/**
	 * @var string
	 */
	protected $numeroPosMaquineta;

	/**
	 * @var string
	 */
	protected $cupomFiscal;

	/**
	 * @var string
	 */
	protected $numeroCaixa;

	/**
	 * @return string
	 */
	public function getNumeroCaixa() {
		return $this->numeroCaixa;
	}

	/**
	 * @param string $numeroCaixa
	 * @return $this
	 */
	public function setNumeroCaixa( $numeroCaixa ) {
		$this->numeroCaixa = $numeroCaixa;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getCupomFiscal() {
		return $this->cupomFiscal;
	}

	/**
	 * @param string $cupomFiscal
	 * @return $this
	 */
	public function setCupomFiscal( $cupomFiscal ) {
		$this->cupomFiscal = $cupomFiscal;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getNumeroPosMaquineta() {
		return $this->numeroPosMaquineta;
	}

	/**
	 * @param string $numeroPosMaquineta
	 * @return $this
	 */
	public function setNumeroPosMaquineta( $numeroPosMaquineta ) {
		$this->numeroPosMaquineta = $numeroPosMaquineta;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getNumeroPdvMaquineta() {
		return $this->numeroPdvMaquineta;
	}

	/**
	 * @param string $numeroPdvMaquineta
	 * @return $this
	 */
	public function setNumeroPdvMaquineta( $numeroPdvMaquineta ) {
		$this->numeroPdvMaquineta = $numeroPdvMaquineta;
		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getNomeOperadorCaixa() {
		return $this->nomeOperadorCaixa;
	}

	/**
	 * @param mixed $nomeOperadorCaixa
	 * @return $this
	 */
	public function setNomeOperadorCaixa( $nomeOperadorCaixa ) {
		$this->nomeOperadorCaixa = $nomeOperadorCaixa;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getProgramaPromocional() {
		return $this->programaPromocional;
	}

	/**
	 * @param string $programaPromocional
	 * @return $this
	 */
	public function setProgramaPromocional( $programaPromocional ) {
		$this->programaPromocional = $programaPromocional;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getNomeProprietario() {
		return $this->nomeProprietario;
	}

	/**
	 * @param string $nomeProprietario
	 * @return $this
	 */
	public function setNomeProprietario( $nomeProprietario ) {
		$this->nomeProprietario = $nomeProprietario;
		return $this;
	}


	/**
	 * Valor de 0 a 100%
	 *
	 * @return float
	 */
	public function getTaxa() {
		return $this->taxa;
	}

	/**
	 *  Valor de 0 a 100%
	 *
	 * @param float $taxa
	 * @return $this
	 */
	public function setTaxa( $taxa ) {
		$this->taxa = $taxa;
		return $this;
	}

	/**
	 * @return float
	 */
	public function getValorBruto() {
		return $this->valorBruto;
	}

	/**
	 * @param float $valorBruto
	 * @return $this
	 */
	public function setValorBruto( $valorBruto ) {
		$this->valorBruto = $valorBruto;
		return $this;
	}


	/**
	 * @return string
	 */
	public function getNumeroPedido() {
		return $this->numeroPedido;
	}

	/**
	 * @param string $numeroPedido
	 * @return $this
	 */
	public function setNumeroPedido( $numeroPedido ) {
		$this->numeroPedido = $numeroPedido;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getCodigoAutorizacao() {
		return $this->codigoAutorizacao;
	}

	/**
	 * @param string $codigoAutorizacao
	 * @return $this
	 */
	public function setCodigoAutorizacao( $codigoAutorizacao ) {
		$this->codigoAutorizacao = $codigoAutorizacao;
		return $this;
	}

	/**
	 * @return Carbon
	 */
	public function getDataVenda() {
		return $this->dataVenda;
	}

	/**
	 * @param Carbon $dataVenda
	 * @return $this
	 */
	public function setDataVenda( $dataVenda ) {
		$this->dataVenda = $dataVenda;
		return $this;
	}

	/**
	 * @return int
	 */
	public function getParcelas() {
		return $this->parcelas;
	}

	/**
	 * @param int $parcelas
	 * @return $this
	 */
	public function setParcelas( $parcelas ) {
		$this->parcelas = $parcelas;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getNumeroCartao() {
		return $this->numeroCartao;
	}

	/**
	 * @param mixed $numeroCartao
	 * @return $this
	 */
	public function setNumeroCartao( $numeroCartao ) {
		$this->numeroCartao = $numeroCartao;
		return $this;
	}

	/**
	 * @return BandeiraEnum
	 */
	public function getBandeira() {
		return $this->bandeira;
	}

	/**
	 * @param BandeiraEnum $bandeira
	 * @return $this
	 */
	public function setBandeira( BandeiraEnum $bandeira ) {
		$this->bandeira = $bandeira;
		return $this;
	}


	/**
	 * @return mixed
	 */
	public function getNsu() {
		return $this->nsu;
	}

	/**
	 * @param mixed $nsu
	 * @return $this
	 */
	public function setNsu( $nsu ) {
		$this->nsu = $nsu;
		return $this;
	}

	/**
	 * @return MeioCapturaEnum
	 */
	public function getMeioCaptura() {
		return $this->meioCaptura;
	}

	/**
	 * @param MeioCapturaEnum $meioCaptura
	 * @return $this
	 */
	public function setMeioCaptura( $meioCaptura ) {
		$this->meioCaptura = $meioCaptura;
		return $this;
	}
}