<?php
namespace Netunna\Cnab\Common\TeiaCard\v0107;

use Netunna\Cnab\Common\TeiaCard\Enum\TipoServicoEnum;

class Lote {
	/**
	 * @var Segmento
	 */
	protected $segmentos;
	/**
	 * @var TipoServicoEnum
	 */
	protected $tipoServico;

	/**
	 * @return TipoServicoEnum
	 */
	public function getTipoServico() {
		return $this->tipoServico;
	}

	/**
	 * @param TipoServicoEnum $tipoServico
	 * @return $this
	 */
	public function setTipoServico( $tipoServico ) {
		$this->tipoServico = $tipoServico;
		return $this;
	}


	/**
	 * @return Segmento
	 */
	public function getSegmentos() {
		return $this->segmentos;
	}

	/**
	 * @param Segmento $segmentos
	 * @return $this
	 */
	public function setSegmentos( $segmentos ) {
		$this->segmentos = $segmentos;
		return $this;
	}

	/**
	 * @param Segmento $segmento
	 * @return $this
	 */
	public function addSegment( Segmento $segmento ) {
		if ( $this->segmentos == null )
			$this->segmentos = [ ];
		$this->segmentos[] = $segmento;
		return $this;
	}


}