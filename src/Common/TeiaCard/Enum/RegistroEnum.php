<?php
namespace Netunna\Cnab\Common\TeiaCard\Enum;

use Netunna\Cnab\Support\Enumerate;

/**
 * Class RegistroEnum
 * @method static RegistroEnum HeaderArquivo()
 * @method static RegistroEnum HeaderLote()
 * @method static RegistroEnum Segmento()
 * @method static RegistroEnum TrailerLote()
 * @method static RegistroEnum TrailerArquivo()
 *
 * @package Netunna\Cnab\Common\TeiaCard\Enum
 */
class RegistroEnum extends Enumerate {
	const HeaderArquivo = '0';              // Header de Arquivo
	const HeaderLote = '1';                 // Header de Lote
	const Segmento = '2';                   // Detalhe - Segmento
	const TrailerLote = '3';                // Trailer de Lote
	const TrailerArquivo = '9';             // Trailer de Arquivo
}
