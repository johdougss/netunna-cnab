<?php
namespace Netunna\Cnab\Common\TeiaCard\Enum;

use Netunna\Cnab\Support\Enumerate;

/**
 * Class ArquivoEnum
 * @method static ArquivoEnum Remessa()
 * @method static ArquivoEnum Retorno()
 *
 * @package Netunna\Cnab\Common\TeiaCard\Enum
 */
class ArquivoEnum extends Enumerate {
	/**
	 * ERP -> Teia-Card
	 */
	const Remessa = '1';
	/**
	 * Teia-Card -> ERP
	 */
	const Retorno = '2';
}
