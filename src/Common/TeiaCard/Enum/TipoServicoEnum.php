<?php
namespace Netunna\Cnab\Common\TeiaCard\Enum;

use Netunna\Cnab\Support\Enumerate;

/**
 * Class TipoServicoEnum
 * @method static TipoServicoEnum VendasCreditoAVista()
 * @method static TipoServicoEnum VendasCreditoParcelado()
 * @method static TipoServicoEnum VendasDebitoAVista()
 * @method static TipoServicoEnum VendasDebitoPreDatado()
 * @method static TipoServicoEnum EstornosCreditoAVista()
 * @method static TipoServicoEnum EstornosCreditoParcelado()
 * @method static TipoServicoEnum EstornosDebitoPreDatado()
 * @method static TipoServicoEnum BaixasParcelaVendasCreditoAVista()
 * @method static TipoServicoEnum BaixasParcelaVendasCreditoParcelado()
 * @method static TipoServicoEnum BaixasParcelaVendasDebitoPreDatado()
 *
 * @package Netunna\Cnab\Common\TeiaCard\Enum
 */
class TipoServicoEnum extends Enumerate {
	const VendasCreditoAVista = '01'; //(Rotativo)
	const VendasCreditoParcelado = '02';
	const VendasDebitoAVista = '03';
	const VendasDebitoPreDatado = '04';
	const EstornosCreditoAVista = '05';//(Rotativo)
	const EstornosCreditoParcelado = '06';
	const EstornosDebitoPreDatado = '07';
	const BaixasParcelaVendasCreditoAVista = '08';//(Rotativo)
	const BaixasParcelaVendasCreditoParcelado = '09';
	const BaixasParcelaVendasDebitoPreDatado = '10';
}