<?php
namespace Netunna\Cnab\Common\TeiaCard\Enum;

use Netunna\Cnab\Support\Enumerate;

/**
 * Class InscricaoEmpresaTipoEnum
 * @method static InscricaoEmpresaTipoEnum IsentoNaoInformado()
 * @method static InscricaoEmpresaTipoEnum Cpf()
 * @method static InscricaoEmpresaTipoEnum CgcCnpj()
 * @method static InscricaoEmpresaTipoEnum PisPasep()
 * @method static InscricaoEmpresaTipoEnum Outros()
 *
 * @package Netunna\Cnab\Common\TeiaCard\Enum
 */
class InscricaoEmpresaTipoEnum extends Enumerate {
	const IsentoNaoInformado = 0;
	const Cpf = 1;
	const CgcCnpj = 2;
	const PisPasep = 3;
	const Outros = 9;
}