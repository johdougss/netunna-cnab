<?php
namespace Netunna\Cnab\Common\TeiaCard\Enum;

use Netunna\Cnab\Support\Enumerate;

/**
 * Class MotivoEstornoEnum
 * @method static MotivoEstornoEnum Troca()
 * @method static MotivoEstornoEnum DevolucaoParcial()
 * @method static MotivoEstornoEnum DevolucaoTotal()
 * @method static MotivoEstornoEnum FaltaDeMercadoria()
 * @method static MotivoEstornoEnum CancelamentoPedido()
 * @method static MotivoEstornoEnum ExtravioRoubo()
 * @method static MotivoEstornoEnum IndenizacaoFrete()
 * @method static MotivoEstornoEnum EstornoPromocao()
 * @method static MotivoEstornoEnum VendaAutorizadaPelaAdquirenteEmDuplicidade()
 * @method static MotivoEstornoEnum VendaCanceladaPorErroDeComunicacaoNaAutorizacao()
 *
 * @package Netunna\Cnab\Common\TeiaCard\Enum
 */
class MotivoEstornoEnum extends Enumerate {
	const Troca = '01';
	const DevolucaoParcial = '02';
	const DevolucaoTotal = '03';
	const FaltaDeMercadoria = '04';
	const CancelamentoPedido = '05';
	const ExtravioRoubo = '06';
	const IndenizacaoFrete = '07';
	const EstornoPromocao = '08';
	const VendaAutorizadaPelaAdquirenteEmDuplicidade = '09';
	const VendaCanceladaPorErroDeComunicacaoNaAutorizacao = '10';
}
