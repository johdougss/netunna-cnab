<?php
namespace Netunna\Cnab\Common\TeiaCard\Enum;

use Netunna\Cnab\Support\Enumerate;

/**
 * Class MeioCapturaEnum
 * @method static MeioCapturaEnum Pos()
 * @method static MeioCapturaEnum Pdv()
 * @method static MeioCapturaEnum ECommerce()
 * @method static MeioCapturaEnum Edi()
 * @method static MeioCapturaEnum AdpBsp()
 * @method static MeioCapturaEnum Manual()
 * @method static MeioCapturaEnum UraCva()
 * @method static MeioCapturaEnum Mobile()
 * @method static MeioCapturaEnum To()
 * @method static MeioCapturaEnum MoedeiroEletronicoRede()
 *
 * @package Netunna\Cnab\Common\TeiaCard\Enum
 */
class MeioCapturaEnum extends Enumerate {
	const Pos = '01';
	const Pdv = '02';
	const ECommerce = '03';
	const Edi = '04';
	const AdpBsp = '05';
	const Manual = '06';
	const UraCva = '07';
	const Mobile = '08';
	const To = '09';
	const MoedeiroEletronicoRede = '10';
}
