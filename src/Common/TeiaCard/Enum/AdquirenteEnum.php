<?php
namespace Netunna\Cnab\Common\TeiaCard\Enum;

use Netunna\Cnab\Support\Enumerate;

/**
 * Class AdquirenteEnum
 * @method static AdquirenteEnum Cielo()
 * @method static AdquirenteEnum RedeCard()
 * @method static AdquirenteEnum Amex()
 * @method static AdquirenteEnum HiperCard()
 * @method static AdquirenteEnum GetNetSantander()
 * @method static AdquirenteEnum Banrisul()
 * @method static AdquirenteEnum Elavon()
 * @method static AdquirenteEnum Stone()
 * @method static AdquirenteEnum Conductor()
 *
 * @package Netunna\Cnab\Common\TeiaCard\Enum
 */
class AdquirenteEnum extends Enumerate {
	const Cielo = 'CIELO';
	const RedeCard = 'REDE (REDECARD)';
	const Amex = 'AMEX';
	const HiperCard = 'HIPERCARD';
	const GetNetSantander = 'GETNET - SANTANDER';
	const Banrisul = 'LOSANGO';
	const Elavon = 'ELAVON';
	const Stone = 'STONE';
	const Conductor = 'CONDUCTOR - Private Label';
}

