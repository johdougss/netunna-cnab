<?php
namespace Netunna\Cnab\Common\TeiaCard\Enum;

use Netunna\Cnab\Support\Enumerate;

/**
 * Class BandeiraEnum
 * @method static BandeiraEnum Visa()
 * @method static BandeiraEnum Mastercard()
 * @method static BandeiraEnum AmericanExpress()
 * @method static BandeiraEnum Hipercard()
 * @method static BandeiraEnum Diners()
 * @method static BandeiraEnum Elo()
 * @method static BandeiraEnum Agiplan()
 * @method static BandeiraEnum Banescard()
 * @method static BandeiraEnum Cabal()
 * @method static BandeiraEnum CredSystem()
 * @method static BandeiraEnum Esplanada()
 * @method static BandeiraEnum CredZ()
 * @method static BandeiraEnum SoroCred()
 * @method static BandeiraEnum Hiper()
 * @method static BandeiraEnum Discover()
 * @method static BandeiraEnum Aurora()
 * @method static BandeiraEnum CooperCred()
 * @method static BandeiraEnum SiCredi()
 * @method static BandeiraEnum AVista()
 * @method static BandeiraEnum Mais()
 * @method static BandeiraEnum UnionPay()
 * @method static BandeiraEnum Alelo()
 * @method static BandeiraEnum GoodCard()
 * @method static BandeiraEnum Jcb()
 * @method static BandeiraEnum Sodexo()
 * @method static BandeiraEnum BonusCba()
 * @method static BandeiraEnum Sapore()
 * @method static BandeiraEnum PoliCard()
 * @method static BandeiraEnum VeroCheque()
 * @method static BandeiraEnum GreenCard()
 * @method static BandeiraEnum ValeCard()
 * @method static BandeiraEnum VrBeneficio()
 *
 * @package Netunna\Cnab\Common\TeiaCard\Enum
 */
class BandeiraEnum extends Enumerate {
	const Visa = '001';
	const Mastercard = '002';
	const AmericanExpress = '003';
	const Hipercard = '004';
	const Diners = '005';
	const Elo = '006';
	const Agiplan = '007';
	const Banescard = '008';
	const Cabal = '009';
	const CredSystem = '010';
	const Esplanada = '011';
	const CredZ = '012';
	const SoroCred = '013';
	const Hiper = '014';
	const Discover = '015';
	const Aurora = '016';
	const CooperCred = '017';
	const SiCredi = '018';
	const AVista = '019';
	const Mais = '020'; //Mais!
	const UnionPay = '021';
	const Alelo = '022';
	const GoodCard = '023';
	const Jcb = '024';
	const Sodexo = '025';
	const BonusCba = '026';
	const Sapore = '027';
	const PoliCard = '028';
	const VeroCheque = '029';
	const GreenCard = '030';
	const ValeCard = '031';
	const VrBeneficio = '032';
}
