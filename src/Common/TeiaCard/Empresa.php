<?php
namespace Netunna\Cnab\Common\TeiaCard;

use Netunna\Cnab\Common\TeiaCard\Enum\InscricaoEmpresaTipoEnum;

class Empresa {
	/** @var  int */
	protected $codigo;
	/** @var  string */
	protected $nome;
	/** @var  string */
	protected $numeroInscricao;
	/** @var  InscricaoEmpresaTipoEnum */
	protected $tipoInscricao;

	//<editor-fold desc="Getters and Setters">
	/**
	 * @return int
	 */
	public function getCodigo() {
		return $this->codigo;
	}

	/**
	 * @param int $codigo
	 * @return $this
	 */
	public function setCodigo( $codigo ) {
		$this->codigo = $codigo;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getNome() {
		return $this->nome;
	}

	/**
	 * @param string $nome
	 * @return $this
	 */
	public function setNome( $nome ) {
		$this->nome = $nome;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getNumeroInscricao() {
		return $this->numeroInscricao;
	}

	/**
	 * @param string $numeroInscricao
	 * @return $this
	 */
	public function setNumeroInscricao( $numeroInscricao ) {
		$this->numeroInscricao = $numeroInscricao;
		return $this;
	}

	/**
	 * @return InscricaoEmpresaTipoEnum
	 */
	public function getTipoInscricao() {
		return $this->tipoInscricao;
	}

	/**
	 * @param InscricaoEmpresaTipoEnum $tipoInscricao
	 * @return $this
	 */
	public function setTipoInscricao( $tipoInscricao ) {
		$this->tipoInscricao = $tipoInscricao;
		return $this;
	}
	//</editor-fold>
}
