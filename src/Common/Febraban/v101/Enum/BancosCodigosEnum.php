<?php
namespace Netunna\Cnab\Template\Febraban\v101\Enum;

use Netunna\Cnab\Support\Enumerate;

class BancosCodigosEnum extends Enumerate {
	const BancoDoBrasilCodigo = 1;
	const SantanderCodigo = 33;
	const CaixaEconomicaFederalCodigo = 104;
	const BradescoCodigo = 237;
	const ItauCodigo = 341;
}