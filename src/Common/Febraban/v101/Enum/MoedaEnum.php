<?php
namespace Netunna\Cnab\Template\Febraban\v101\Enum;

use Netunna\Cnab\Support\Enumerate;

class MoedaEnum extends Enumerate {
	const BTN = 'BTN'; //Bônus do Tesouro Nacional + TR
	const BRL = 'BRL'; //Real
	const USD = 'USD'; //Dólar Americano
	const PTE = 'PTE'; //Escudo Português
	const FRF = 'FRF'; //Franco Francês
	const CHF = 'CHF'; //Franco Suíço
	const JPY = 'JPY'; //Ien Japonês
	const IGP = 'IGP'; //Índice Geral de Preços
	const IGM = 'IGM'; //Índice Geral de Preços de Mercado
	const GBP = 'GBP'; //Libra Esterlina
	const ITL = 'ITL'; //Lira Italiana
	const DEM = 'DEM'; //Marco Alemão
	const TRD = 'TRD'; //Taxa Referencial Diária
	const UPC = 'UPC'; //Unidade Padrão de Capital
	const UPF = 'UPF'; //Unidade Padrão de Financiamento
	const UFR = 'UFR'; //Unidade Fiscal de Referência
	const XEU = 'XEU'; //Unidade Monetária Européia
}
