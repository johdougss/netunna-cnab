<?php
namespace Netunna\Cnab\Template\Febraban\v101\Enum;

use Netunna\Cnab\Support\Enumerate;

class BancoNomesEnumerate extends Enumerate {

	const BancoDoBrasilNome = 'BANCO DO BRASIL S.A.';
	const SantanderNome = 'BANCO SANTANDER (BRASIL) S/A';
	const CaixaEconomicaFederalNome = 'CAIXA ECONOMICA FEDERAL';
	const BradescoNome = 'BRADESCO';
	const ItauNome = 'BANCO ITAU SA';

	protected static $bancos = [
		BancosCodigosEnum::BancoDoBrasilCodigo => self::BancoDoBrasilNome,
		BancosCodigosEnum::SantanderCodigo => self::SantanderNome,
		BancosCodigosEnum::CaixaEconomicaFederalCodigo => self::CaixaEconomicaFederalNome,
		BancosCodigosEnum::BradescoCodigo => self::BradescoNome,
		BancosCodigosEnum::ItauCodigo => self::ItauNome,
	];

	public static function getBancoByCodigo( BancosCodigosEnum $codigo ) {
		isset( self::$bancos[ $codigo->getValue() ] ) ? self::$bancos[ $codigo->getValue() ] : null;
	}
}