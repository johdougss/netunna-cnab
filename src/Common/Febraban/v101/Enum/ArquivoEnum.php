<?php
namespace Netunna\Cnab\Template\Febraban\v101\Enum;

use Netunna\Cnab\Support\Enumerate;

class ArquivoEnum extends Enumerate {

	/**
	 *  Cliente -> Banco
	 */
	const Remessa = '1';
	/**
	 * Banco -> Cliente
	 */
	const Retorno = '2';
}
