<?php
namespace Netunna\Cnab\Template\Febraban\v101\Enum;

use Netunna\Cnab\Support\Enumerate;

class RegistroEnum extends Enumerate {

	const HeaderArquivo = '0';             // Header de Arquivo
	const HeaderLote = '1';                // Header de Lote
	const RegistrosIniciaisDoLote = '2';   // Registros Iniciais do Lote
	const Detalhe = '3';                   // Detalhe
	const RegistrosFinaisDoLote = '4';     // Registros Finais do Lote
	const TrailerLote = '5';               // Trailer de Lote
	const TrailerArquivo = '9';            // Trailer de Arquivo
}
