<?php
namespace Netunna\Cnab\Template\Febraban\v101\Enum;

use Netunna\Cnab\Support\Enumerate;

class SituacaoSaldoEnum extends Enumerate {

	const Devedor = 'D';
	const Credor = 'C';
}
