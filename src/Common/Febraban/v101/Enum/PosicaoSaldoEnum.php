<?php
namespace Netunna\Cnab\Template\Febraban\v101\Enum;

use Netunna\Cnab\Support\Enumerate;

class PosicaoSaldoEnum extends Enumerate {

	const SaldoFinal = 'F';
	const SaldoParcial = 'P';
	const SaldoIntraDia = 'I';
}
