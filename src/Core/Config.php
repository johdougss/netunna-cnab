<?php
namespace Netunna\Cnab\Core;

use Illuminate\Filesystem\Filesystem;
use Netunna\Cnab\Core\Builder\FileBuilder;
use Netunna\Cnab\Core\Builder\LineBuilder;
use Netunna\Cnab\Core\Builder\LotBuilder;
use Netunna\Cnab\Core\Exception\InvalidTemplateException;
use Symfony\Component\Yaml\Yaml;

class Config {
	/** @var string */
	protected $path;
	/** @var  string */
	protected $dirname;
	/** @var  ConfigItem[] */
	protected $configItem;
	/** @var array */
	protected $templateNames = [
		ConfigTemplateEnum::TEIA_CARD_V0106 => __DIR__ . '/../templates/teia-card/v0106/config.yml',
		ConfigTemplateEnum::TEIA_CARD_V0107 => __DIR__ . '/../templates/teia-card/v0107/config.yml',
		ConfigTemplateEnum::BRADESCO_05 => __DIR__ . '/../templates/bradesco/05/config.yml',
	];

	/**
	 * @param $name
	 * @param $path
	 */
	public function addTemplateName( $name, $path ) {
		$this->templateNames[ $name ] = $path;
	}

	/**
	 * @param string|ConfigTemplateEnum $templateName
	 */
	public function load( $templateName ) {
		if ( $templateName instanceof ConfigTemplateEnum ) {
			$templateName = $templateName->getValue();
		}
		$this->loadPath( $this->getTemplatePath( $templateName ) );
	}

	/**
	 * Busca um template
	 *
	 * @param string $templateName
	 * @return string
	 */
	public function getTemplatePath( $templateName ) {
		if ( !isset( $this->templateNames[ $templateName ] ) )
			new InvalidTemplateException( "O template '$templateName' não está definido nas configuracoes" );
		return $this->templateNames[ $templateName ];
	}

	/**
	 * Carrega o arquivo de configuracoes
	 *
	 * @param string $path
	 * @throws InvalidTemplateException
	 */
	public function loadPath( $path = null ) {

		$file = new Filesystem();
		if ( !$file->exists( $path ) )
			throw new InvalidTemplateException( "nenhum arquivo encontrado '$path'" );

		$this->path = $path;
		$this->dirname = pathinfo( $this->path )[ 'dirname' ];

		$conteudo = $this->loadXmlFile( $path );

		if ( !isset( $conteudo[ 'templates' ] ) ) {
			throw new InvalidTemplateException( "nenhum template definido para ser carregado" );
		}

		foreach ( $conteudo[ 'templates' ] as $template ) {
			$templateModel = ConfigItem::create( $template );

			if ( $templateModel->getFilename() == null ) {
				throw new InvalidTemplateException( 'nome do arquivo não especificado' );
			}

			$path = $this->dirname . DIRECTORY_SEPARATOR . $templateModel->getFilename();
			$camposArray = $this->loadXmlFile( $path );

			try {
				$templateModel->setLength( $conteudo[ 'length' ] );
				$templateModel->setVersion( $conteudo[ 'version' ] );
				$templateModel->setFieldsArray( $camposArray );
				$templateModel->validate();
			} catch ( InvalidTemplateException $e ) {
				$message = "Erro no template '" . $templateModel->getId() . "'" .
					PHP_EOL . realpath( $path ) .
					PHP_EOL . $e->getMessage();
				throw new InvalidTemplateException( $message );
			}
			$this->configItem[ $templateModel->getId() ] = $templateModel;
		}
	}

	protected function loadXmlFile( $path ) {
		if ( $path == null )
			throw new InvalidTemplateException( 'Path not found.' );

		$file = new Filesystem();
		if ( !$file->exists( $path ) )
			throw new InvalidTemplateException( "File '$path' not exists." );

		$file = new Filesystem();
		return Yaml::parse( $file->get( $path ) );
	}

	public function getConfigItem() {

		return $this->configItem;
	}

	/**
	 * Busca um template
	 *
	 * @param string|int $id
	 * @return ConfigItem
	 */
	public function getTemplateById( $id ) {
		$templates = $this->getConfigItem();
		return isset( $templates[ $id ] ) ? $templates[ $id ] : null;
	}

	/**
	 * @param FileBuilder $arquivoTemplate
	 * @return string
	 * @throws InvalidTemplateException
	 */
	public function encode( FileBuilder $arquivoTemplate ) {
		$buffer = [ ];
		$headerArquivo = $arquivoTemplate->getFileHeader();
		$trailerArquivo = $arquivoTemplate->getFileTrailer();
		/** @var LotBuilder[] $lotes */
		$lotes = $arquivoTemplate->getLots();

		if ( $headerArquivo ) {
			$buffer[] = $this->getTemplateById( '0' )->encode( $headerArquivo );
		}

		if ( $lotes ) {
			foreach ( $lotes as $lote ) {
				$headerLote = $lote->getHeaderLot();
				$trailerLote = $lote->getTrailerLot();
				$segmentos = $lote->getSegments();

				if ( $headerLote != null ) {
					$buffer[] = $this->encodeLine( $headerLote );
				}

				if ( $segmentos != null ) {
					foreach ( $segmentos as $segmento ) {
						$buffer[] = $this->encodeLine( $segmento );
					}
				}

				if ( $trailerLote != null ) {
					$buffer[] = $this->encodeLine( $trailerLote );
				}
			}
		}
		if ( $trailerArquivo ) {
			$buffer[] = $this->encodeLine( $trailerArquivo );
		}

		return $buffer;
	}

	/**
	 * @param $line
	 * @return string
	 * @throws InvalidTemplateException
	 */
	private function encodeLine( LineBuilder $line ) {
		$templateId = $line->getTemplateId();
		if ( $templateId == null )
			throw new InvalidTemplateException( 'O template da linha não foi encontrado' );

		$template = $this->getTemplateById( $templateId );
		if ( $template == null )
			throw new InvalidTemplateException( 'O template "' . $templateId . '" não foi encontrado' );

		return $template->encode( $line );
	}

//	/**
//	 * Decodifica varias linhas
//	 *
//	 * @param string $text
//	 * @return FileBuilder $arquivoTemplate
//	 * @throws InvalidTemplateException
//	 */
//	public function decode( $text ) {
//		$linhas = preg_split( "/\r\n|\n|\r/", $text );
//		$arquivoTemplate = new FileBuilder();
//
//		$loteTemplate = null;
//		$segmentBuilder = null;
//		$segmentoId = null;
//		foreach ( $linhas as $linha ) {
//			if ( $linha == '' )
//				continue;
//
//			$templateById = $templateId = substr( $linha, 7, 1 );
//			if ( $templateId == '2' ) {
//				$segmentoId = substr( $linha, 15, 1 );
//				$templateById = $templateId . $segmentoId;
//			}
//
//
//			$template = $this->getTemplateById( $templateById );
//			if ( $template == null )
//				throw new  InvalidTemplateException( 'O template não foi encontrado' );
//
//			$arrayDecode = $template->decode( $linha );
//			$linhaTemplate = LineBuilder::create( $arrayDecode );
//
//			switch ( $templateId ) {
//				case '0':
//					$arquivoTemplate->setFileHeader( $linhaTemplate );
//					break;
//				case '9':
//					$arquivoTemplate->setFileTrailer( $linhaTemplate );
//					break;
//
//				case '1':
//					if ( $loteTemplate == null )
//						$loteTemplate = new LotBuilder();
//					$loteTemplate->setHeaderLot( $linhaTemplate );
//					break;
//				case '3':
//					if ( $loteTemplate == null )
//						$loteTemplate = new LotBuilder();
//					$loteTemplate->setTrailerLot( $linhaTemplate );
//					$arquivoTemplate->addLot( $loteTemplate );
//					$loteTemplate = null;
//					break;
//
//				case '2':
//					if ( $segmentBuilder == null ) {
//						$segmentBuilder = new SegmentBuilder();
//					}
//					$segmentBuilder->
//
//					$segmentBuilder->( $linhaTemplate );
//					$loteTemplate->addSegments( $linhaTemplate );
//
//			}
//		}
//		return $arquivoTemplate;
//	}

//	/**
//	 * @param $path
//	 * @return FileBuilder array
//	 * @throws InvalidTemplateException
//	 */
//	public function decodeFile( $path ) {
//		$file = new Filesystem();
//		$content = $file->get( $path );
//		return $this->decode( $content );
//	}

	/**
	 * @param string      $path
	 * @param FileBuilder $fileBuilder
	 * @throws InvalidTemplateException
	 */
	public function encodeFile( $path, FileBuilder $fileBuilder ) {
		$buffer = $this->encode( $fileBuilder );
		$file = new Filesystem();
		$file->put( $path, implode( "\n", $buffer ) );
	}

	/**
	 * @param string $path
	 * @param array  $buffer
	 */
	public function encodeFileBuffer( $path, $buffer ) {
		$file = new Filesystem();
		$file->put( $path, implode( "\n", $buffer ) );
	}

}