<?php
namespace Netunna\Cnab\Core;

use Netunna\Cnab\Core\Builder\LineBuilder;
use Netunna\Cnab\Core\Exception\InvalidTemplateException;

class ConfigItem {
	/** @var  int */
	protected $id;
	/** @var  string */
	protected $filename;
	/** @var  string */
	protected $description;
	/** @var Field[] */
	protected $fields = [ ];

	/**
	 * Quantidade de caracteres por linha
	 *
	 * @var int
	 */
	protected $length;

	/** @var  string */
	protected $version;
	// <editor-fold desc="Getters and Setters">
	/**
	 * @return int
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * @param int $id
	 * @return $this
	 */
	public function setId( $id ) {
		$this->id = $id;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getFilename() {
		return $this->filename;
	}

	/**
	 * @param string $filename
	 * @return $this
	 */
	public function setFilename( $filename ) {
		$this->filename = $filename;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getDescription() {
		return $this->description;
	}

	/**
	 * @param string $description
	 * @return $this
	 */
	public function setDescription( $description ) {
		$this->description = $description;
		return $this;
	}

	/**
	 * @param Field[] $fields
	 * @return $this
	 */
	public function setFields( $fields ) {
		$this->fields = $fields;
		return $this;
	}

	/**
	 * @return Field[]
	 */
	public function getFields() {
		return $this->fields;
	}

	/**
	 * @return int
	 */
	public function getLength() {
		return $this->length;
	}

	/**
	 * @param int $length
	 * @return $this
	 */
	public function setLength( $length ) {
		$this->length = $length;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getVersion() {
		return $this->version;
	}

	/**
	 * @param string $version
	 * @return $this
	 */
	public function setVersion( $version ) {
		$this->version = $version;
		return $this;
	}
// </editor-fold>

	/**
	 * @param int $id
	 * @return Field|null
	 */
	public function getFieldById( $id ) {
		return isset( $this->fields[ $id ] ) ? $this->fields[ $id ] : null;
	}

	/**
	 * @param array $fieldsArray
	 * @return $this
	 * @throws InvalidTemplateException
	 */
	public function setFieldsArray( array $fieldsArray ) {

		foreach ( $fieldsArray as $key => $fieldArr ) {
			$field = new Field();
			$field->fill( $fieldArr );
			$this->fields[ $key ] = $field;
		}
		return $this;
	}

	/**
	 * @param array $arr
	 * @return array
	 */
	public function getStructure( array $arr ) {
		$result = [ ];
		foreach ( $this->fields as $key => $field ) {
			$result[ $key ] = null;
			if ( isset( $arr[ $key ] ) )
				$result[ $key ] = $arr[ $key ];
		}
		return $result;
	}

	/**
	 *  Decodifica uma linha por voces
	 *
	 * @param string $linha
	 * @return array
	 * @throws InvalidTemplateException
	 */
	public function decode( $linha ) {
		$arr = [ ];
		foreach ( $this->getFields() as $key => $campo ) {
			try {
				$valueStr = substr( $linha, $campo->getPositionStart() - 1, $campo->getPosicaoLength() );
				$value = $campo->decode( $valueStr );
				$arr[ $key ] = $value;
			} catch ( InvalidTemplateException $e ) {
				$mensagem =
					"Erro na linha '" . preg_replace( "/[\n|\r]+/", "", $linha ) . "':\n" .
					"Erro no decode do campo '$key':\n" .
					"Posição: [" . $campo->getPositionStart() . "," . $campo->getPositionEnd() . "]\n" .
					$e->getMessage();
				throw new InvalidTemplateException( $mensagem );
			}
		}
		return $arr;
	}

	public function validate() {
		self::validCollision( $this );
		self::validLengthPicture( $this );
	}

	/**
	 * Verifica se o tamanho informado na picture esta de acordo com o a diferença entre as posições.
	 *
	 * @see Picture
	 * @param ConfigItem $template
	 * @throws InvalidTemplateException
	 */
	public static function validLengthPicture( ConfigItem $template ) {
		$error = [ ];
		foreach ( $template->getFields() as $key => $campo ) {
			if ( $campo->getPosicaoLength() != $campo->getPicture()->getFormatLength() ) {
				$error [ $key ] = [
					'posicao' => [ $campo->getPositionStart(), $campo->getPositionEnd() ],
					'picture' => $campo->getPicture()->getFormat(),
					'tamanho' => $campo->getPosicaoLength(),
					'picture-tamanho' => $campo->getPicture()->getFormatLength()
				];

				throw new InvalidTemplateException( "No campo '$key': o tamanho entre as posições[" . $campo->getPositionStart() . "," .
					$campo->getPositionEnd() . "] é igual a '" . $campo->getPosicaoLength() .
					"' e não corresponde ao tamanho total especificado no formato '" . $campo->getPicture()->getFormat() . "'" );
			}
		}
		if ( !empty( $error ) )
			throw new InvalidTemplateException( 'O tamanho entre as posições nao corresponde ao tamanho total especificado no formato(picture).  [' . print_r( $error, true ) . ']' );
	}

	/**
	 * Valida a posições do template, garantindo que todos campos foram mapeados e não a choque de posição
	 *
	 * @see Picture
	 * @param ConfigItem $template
	 * @throws InvalidTemplateException
	 */
	public static function validCollision( ConfigItem $template ) {

		$colisao = array_fill( 0, $template->getLength(), -1 );
		foreach ( $template->getFields() as $key => $campo ) {
			for ( $i = $campo->getPositionStart() - 1; $i < $campo->getPositionEnd(); $i++ ) {
				if ( !array_key_exists( $i, $colisao ) ) {
					throw new InvalidTemplateException( 'Erro no tamanho especificado, verificar o campo: ' . $key );
				}
				$colisao[ $i ]++;
			}
		}
		$error_colisao = [ ];
		$error_preenchimento = [ ];
		foreach ( $colisao as $key => $value ) {
			if ( $value > 0 ) {
				$error_colisao[] = $key + 1;
			} else if ( $value < 0 ) {
				$error_preenchimento[] = $key + 1;
			}
		}
		if ( !empty( $error_colisao ) ) {
			throw new InvalidTemplateException( 'As posições a seguir foram consultadas mais que uma vez [' . print_r( $error_colisao, true ) . ']' );
		}

		if ( !empty( $error_preenchimento ) ) {
			throw new InvalidTemplateException( 'As posições a seguir não foram consultadas [' . print_r( $error_preenchimento, true ) . ']' );
		}
	}

	/**
	 * @param $arr
	 * @return ConfigItem
	 */
	public static function create( $arr ) {
		$instance = new ConfigItem();
		$instance->id = array_get( $arr, 'id' );
		$instance->description = array_get( $arr, 'description' );
		$instance->filename = array_get( $arr, 'filename' );
		return $instance;
	}

	/**
	 * @param LineBuilder $linha
	 * @return string
	 * @throws InvalidTemplateException
	 */
	public function encode( LineBuilder $linha ) {
		$str = str_pad( '', 240 );

		foreach ( $linha->getUnits() as $unidade ) {
			$campo = $this->getFieldById( $unidade->getKey() );
			$campo->setKey( $unidade->getKey() );

			if ( $campo == null )
				throw new InvalidTemplateException( "Campo '" . $unidade->getKey() . "' não encontrado " );

			try {
				$encode = $campo->encode( $unidade->getValue() );
			} catch ( InvalidTemplateException $e ) {
				throw new InvalidTemplateException( "Erro no encode do campo '" . $unidade->getKey() . "':\n" . $e->getMessage() );
			}
			$str = substr_replace( $str, $encode, $campo->getPositionStart() - 1, $campo->getPosicaoLength() );
		}

		return $str;

	}
}
