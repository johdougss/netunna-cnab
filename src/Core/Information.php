<?php
namespace Netunna\Cnab\Core;

class Information {

	/** @var  string */
	protected $formatoData;
	/** @var  string */
	protected $formatoTempo;
	/** @var  int */
	protected $tamanho;

	//<editor-fold desc="Getters and Setters">
	/**
	 * @return string
	 */
	public function getFormatoData() {
		return $this->formatoData;
	}

	/**
	 * @param string $formatoData
	 * @return $this
	 */
	public function setFormatoData( $formatoData ) {
		$this->formatoData = $formatoData;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getFormatoTempo() {
		return $this->formatoTempo;
	}

	/**
	 * @param string $formatoTempo
	 * @return $this
	 */
	public function setFormatoTempo( $formatoTempo ) {
		$this->formatoTempo = $formatoTempo;
		return $this;
	}

	/**
	 * @return int
	 */
	public function getTamanho() {
		return $this->tamanho;
	}

	/**
	 * @param int $tamanho
	 * @return $this
	 */
	public function setTamanho( $tamanho ) {
		$this->tamanho = $tamanho;
		return $this;
	}
	//</editor-fold>

}