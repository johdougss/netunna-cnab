<?php
namespace Netunna\Cnab\Core;

use Netunna\Cnab\Support\Enumerate;

class ConfigTemplateEnum extends Enumerate {
	const TEIA_CARD_V0106 = 'TEIA_CARD_V0106';
	const TEIA_CARD_V0107 = 'TEIA_CARD_V0107';
	const BRADESCO_05 = 'BRADESCO_05';
}

