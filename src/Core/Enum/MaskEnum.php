<?php
namespace Netunna\Cnab\Core\Enum;

use Netunna\Cnab\Support\Enumerate;

class MaskEnum extends Enumerate {
	const Time = 'time';
	const Date = 'date';
	const DateTime = 'datetime';
	const Float = 'float';
	const Int = 'int';
	const String = 'string';
}
