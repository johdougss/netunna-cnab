<?php
namespace Netunna\Cnab\Core\Enum;

use Netunna\Cnab\Support\Enumerate;

/**
 * Class SegmentoEnum
 * @method static SegmentEnum V()
 * @method static SegmentEnum B()
 * @method static SegmentEnum X()
 *
 * @package Netunna\Cnab\Common\TeiaCard\Enum
 */
class SegmentEnum extends Enumerate {
	const V = 'V';
	const P = 'P';
}
