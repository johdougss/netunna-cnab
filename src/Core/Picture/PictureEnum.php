<?php

namespace Netunna\Cnab\Core\Picture;

use Netunna\Cnab\Support\Enumerate;

class PictureEnum extends Enumerate {
	const Alfanumerico = 'X';
	const Numerico = '9';
}