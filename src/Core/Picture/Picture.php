<?php

namespace Netunna\Cnab\Core\Picture;

use Netunna\Cnab\Core\Exception\InvalidTemplateException;


/**
 * Você só precisa informar a picture, o CnabPHP já converte o formato para você (tanto no retorno como na remessa).
 * Cada registro é formado por campos que são apresentados em dois formatos:
 * Alfanumérico (picture X)
 * Alinhados à esquerda com brancos à direita. Preferencialmente, todos os caracteres devem ser maiúsculos.
 * Aconselha-se a não utilização de caracteres especiais (ex.: “Ç”, “?”,, etc) e acentuação gráfica (ex.: “Á”, “É”,
 * “Ê”, etc) e os campos não utilizados deverão ser preenchidos com brancos. Numérico (picture 9) Alinhado à direita
 * com zeros à esquerda e os campos não utilizados deverão ser preenchidos com zeros. Vírgula assumida (picture V):
 * indica a posição da vírgula dentro de um campo numérico. Exemplo: num campo com picture “9(5)V9(2)”, o número
 * “876,54” será representado por “0087654”. Picture    | Descrição                                       | Exemplo
 * ----------------------------------------------------------------------------------------
 * X(10)      | Texto de no máximo 10 caracteres                | "TESTE     " == "TESTE"
 * 9(10)V9(2) | Valor decimal 10 posições mais 2 casas decimais | "000000001050" == 10,50
 * 9(12)      | Número com 12 posições                          | "000000000020" == 20
 * Class Picture
 */
class Picture {
	/** @var  string */
	protected $format;
	/** @var  int */
	protected $formatLength;
	/** @var  PictureEnum */
	protected $tipo;
	/** @var array */
	protected $values = [ ];
	/** @var  bool */
	protected $valid;

	public function __construct( $format = null ) {
		if ( $format != null )
			$this->setFormat( $format );
	}

	/**
	 * Verifica o formato da picture é valido
	 *
	 * @return bool
	 */
	public function isFormatValid() {
		if ( $this->valid == null ) {
			$pattern = '/(?:9\([0-9]+\)(?:V9\([0-9]+\))?|X\([0-9]+\))$/';
			$this->valid = preg_match( $pattern, $this->format ) ? true : false;
		}
		return $this->valid;
	}

	/**
	 * Retorna o tipo da picture, (numerico ou alfanumerico)
	 *
	 * @return int
	 * @throws InvalidTemplateException
	 */
	public function getFormatTipo() {
		if ( $this->tipo == null ) {
			$pattern = '/(^[X|9]).*/';
			$subject = '$1';
			$tipo = preg_replace( $pattern, $subject, $this->format );

			if ( $tipo != PictureEnum::Alfanumerico && $tipo != PictureEnum::Numerico )
				throw new InvalidTemplateException( "'$this->format' não é um formato válido" );

			$this->tipo = $tipo;
		}
		return $this->tipo;
	}

	/**
	 * Verifica se o formato é do tipo float
	 *
	 * @return int
	 */
	public function isFormatFloat() {
		return preg_match( '/V/', $this->format ) ? true : false;
	}

	/**
	 * Captura os valores informados na picture
	 *
	 * @return array
	 */
	public function getFormatValues() {
		if ( $this->values == null ) {
			if ( $this->isFormatFloat() ) {
				$output_array = array();
				$pattern = '/\(([0-9]+)\)(?:V9\(([0-9]+)\))?/';
				preg_match( $pattern, $this->format, $output_array );
				return array( $output_array[ 1 ], $output_array[ 2 ] );
			} else {
				$pattern = '/\(([0-9]+)\)/';
				preg_match( $pattern, $this->format, $output_array );
				return array( $output_array[ 1 ] );
			}
		}
		return $this->values;
	}

	/**
	 * Retorna o tamanho do campo
	 *
	 * @return mixed
	 */
	public function getFormatLength() {
		if ( $this->formatLength == null ) {
			if ( $this->isFormatFloat() ) {
				list( $value1, $value2 ) = $this->getFormatValues();
				return $value1 + $value2;
			} else
				return $this->getFormatValues()[ 0 ];
		}
		return $this->formatLength;
	}

	/**
	 * @param $format
	 * @return $this
	 * @throws InvalidTemplateException
	 */
	public function setFormat( $format ) {
		$this->format = $this->normalizeFormat( $format );

		if ( !$this->isFormatValid() )
			throw new InvalidTemplateException( "'$this->format' é um formato inválido" );

		$this->resetCache();
		return $this;
	}

	public function getFormat() {
		return $this->format;
	}

	/**
	 * Padroniza o format para maiusculo
	 *
	 * @param $format
	 * @return string
	 */
	public function normalizeFormat( $format ) {
		return strtoupper( $format );
	}

	private function resetCache() {
		$this->tipo = null;
		$this->values = null;
		$this->valid = null;
		$this->formatLength = null;
	}

	public function validValue( $value ) {
		$str_value = strlen( $value );
		if ( $str_value > $this->getFormatLength() ) {
			throw new InvalidTemplateException( "O valor '$value' não atende ao formato da picture '" . $this->getFormat() . "'" );
		}
		return true;
	}

}