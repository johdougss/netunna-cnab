<?php

namespace Netunna\Cnab\Core\Builder;


use Illuminate\Contracts\Support\Arrayable;

class LineBuilder implements Arrayable {
	protected $templateId;

	/**
	 * @return string
	 */
	public function getTemplateId() {
		return $this->templateId;
	}

	/**
	 * @param string $templateId
	 * @return $this
	 */
	public function setTemplateId( $templateId ) {
		$this->templateId = $templateId;
		return $this;
	}

	/** @var  UnitBuilder[] */
	protected $units;

	/**
	 * @param $arr
	 * @return static
	 */
	public static function create( $arr ) {
		$instance = new static;
		if ( $arr == null ) {
			return $instance;
		}

		foreach ( $arr as $key => $value )
			$instance->addUnit( $key, $value );
		return $instance;
	}

	/**
	 * @return UnitBuilder[]
	 */
	public function getUnits() {
		return $this->units;
	}

	/**
	 * @param UnitBuilder[] $units
	 * @return $this
	 */
	public function setUnits( $units ) {
		$this->units = $units;
		return $this;
	}

	/**
	 * @param int|string $key
	 * @param mixed      $value
	 * @return $this
	 */
	public function addUnit( $key, $value ) {
		$unit = new UnitBuilder();
		$unit->setValue( $value );
		$unit->setKey( $key );

		$this->units[] = $unit;
		return $this;
	}


	/**
	 * @param $id
	 * @return UnitBuilder
	 */
	public function getUnidadeById( $id ) {
		foreach ( $this->units as $unit )
			if ( $unit->getKey() == $id )
				return $unit;
		return null;
	}

	/**
	 * {@inheritdoc}
	 */
	public function toArray() {
		$arr = [ ];
		foreach ( $this->getUnits() as $unit )
			$arr[ $unit->getKey() ] = $unit->getValue();
		return $arr;
	}
}