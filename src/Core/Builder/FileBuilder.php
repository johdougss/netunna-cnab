<?php

namespace Netunna\Cnab\Core\Builder;


use Illuminate\Contracts\Support\Arrayable;

class FileBuilder implements Arrayable {

	/** @var  FileHeaderBuilder */
	protected $fileHeader;
	/** @var  FileTrailerBuilder */
	protected $fileTrailer;
	/** @var LotBuilder[] */
	protected $lots;

	public static function create( $arr ) {
		$instance = new self;

		$instance
			->setFileHeader( array_get( $arr, 'header' ) )
			->setFileTrailer( array_get( $arr, 'trailer' ) );

		foreach ( array_get( $arr, 'lots', [ ] ) as $lote )
			$instance->addLot( LotBuilder::create( $lote ) );
		return $instance;
	}

	/**
	 * @return FileHeaderBuilder
	 */
	public function getFileHeader() {
		return $this->fileHeader;
	}

	/**
	 * @param FileHeaderBuilder|array $fileHeader
	 * @return $this
	 */
	public function setFileHeader( $fileHeader ) {
		if ( is_array( $fileHeader ) )
			$fileHeader = FileHeaderBuilder::create( $fileHeader );
		$this->fileHeader = $fileHeader;
		return $this;
	}

	/**
	 * @return FileTrailerBuilder
	 */
	public function getFileTrailer() {
		return $this->fileTrailer;
	}

	/**
	 * @param FileTrailerBuilder|array $fileTrailer
	 * @return $this
	 */
	public function setFileTrailer( $fileTrailer ) {
		if ( is_array( $fileTrailer ) )
			$fileTrailer = FileTrailerBuilder::create( $fileTrailer );
		$this->fileTrailer = $fileTrailer;
		return $this;
	}

	/**
	 * @return LotBuilder[]
	 */
	public function getLots() {
		return $this->lots;
	}

	/**
	 * @param LotBuilder[] $lots
	 * @return $this
	 */
	public function setLots( $lots ) {
		$this->lots = $lots;
		return $this;
	}

	/**
	 * Add new lote
	 *
	 * @param $lot
	 */
	public function addLot( LotBuilder $lot ) {
		$this->lots[] = $lot;
	}

	/**
	 * {@inheritdoc}
	 */
	public function toArray() {
		$arr = [
			'header' => $this->fileHeader->toArray(),
			'trailer' => $this->fileTrailer->toArray(),
		];

		foreach ( $this->lots as $lot )
			$arr[ 'lots' ][] = $lot->toArray();
		return $arr;
	}
}