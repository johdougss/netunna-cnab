<?php

namespace Netunna\Cnab\Core\Builder;


class UnitBuilder {

	/** @var  string */
	protected $key;
	/** @var  mixed */
	protected $value;

	//<editor-fold desc="Getters and Setters">
	/**
	 * @return string
	 */
	public function getKey() {
		return $this->key;
	}

	/**
	 * @param string $key
	 * @return $this
	 */
	public function setKey( $key ) {
		$this->key = $key;
		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getValue() {
		return $this->value;
	}

	/**
	 * @param mixed $value
	 * @return $this
	 */
	public function setValue( $value ) {
		$this->value = $value;
		return $this;
	}
	//</editor-fold>
}