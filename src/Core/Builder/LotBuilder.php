<?php

namespace Netunna\Cnab\Core\Builder;


use Illuminate\Contracts\Support\Arrayable;

class LotBuilder implements Arrayable {
	/** @var  LotHeaderBuilder */
	protected $headerLot;

	/** @var  LotTrailerBuilder */
	protected $trailerLot;

	/** @var  SegmentBuilder[] */
	protected $segments = [ ];

	//<editor-fold desc="Getters and Setters">
	/**
	 * @return LotHeaderBuilder
	 */
	public function getHeaderLot() {
		return $this->headerLot;
	}

	/**
	 * @param LotHeaderBuilder|array $headerLot
	 * @return LotBuilder
	 */
	public function setHeaderLot( $headerLot ) {
		if ( is_array( $headerLot ) )
			$headerLot = LotHeaderBuilder::create( $headerLot );
		$this->headerLot = $headerLot;
		return $this;
	}

	/**
	 * @return LotTrailerBuilder
	 */
	public function getTrailerLot() {
		return $this->trailerLot;
	}

	/**
	 * @param LotTrailerBuilder|array $trailerLot
	 * @return LotBuilder
	 */
	public function setTrailerLot( $trailerLot ) {
		if ( is_array( $trailerLot ) )
			$trailerLot = LotTrailerBuilder::create( $trailerLot );
		$this->trailerLot = $trailerLot;
		return $this;
	}

	/**
	 * @return SegmentBuilder[]
	 */
	public function getSegments() {
		return $this->segments;
	}

	/**
	 * @param SegmentBuilder[] $segments
	 * @return $this
	 */
	public function setSegments( $segments ) {
		$this->segments = $segments;
		return $this;
	}
	//</editor-fold>

	/**
	 * @param SegmentBuilder $segment
	 * @return $this
	 */
	public function addSegments( $segment ) {
		$this->segments[] = $segment;
		return $this;
	}


	/**
	 * {@inheritdoc}
	 */
	public function toArray() {
		$arr = [
			'header' => $this->headerLot->toArray(),
			'trailer' => $this->trailerLot->toArray(),
		];
		foreach ( $this->segments as $segment ) {
			$arr[ 'segments' ][ $segment->getTemplateId() ][] = $segment->toArray();
		}
		return $arr;
	}

	/**
	 * @param $arr
	 * @return static
	 */
	public static function create( $arr ) {
		$instance = new static;
		$instance->setHeaderLot( LotHeaderBuilder::create( array_get( $arr, 'header' ) ) );
		$instance->setTrailerLot( LotTrailerBuilder::create( array_get( $arr, 'trailer' ) ) );

		foreach ( $arr[ 'segments' ] as $key => $segmentGroup ) {
			foreach ( $segmentGroup as $segment ) {
				$segmentBuilder = SegmentBuilder::create( $segment );
				$segmentBuilder->setTemplateId( $key );
				$instance->addSegments( $segmentBuilder );
			}
		}
		return $instance;
	}
}