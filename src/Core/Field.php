<?php
namespace Netunna\Cnab\Core;

use Carbon\Carbon;
use Netunna\Cnab\Core\Enum\MaskEnum;
use Netunna\Cnab\Core\Exception\InvalidTemplateException;
use Netunna\Cnab\Core\Picture\Picture;
use Netunna\Cnab\Core\Picture\PictureEnum;

class Field {
	/** @var string */
	protected $description;
	/** @var Picture */
	protected $picture;
	/** @var int */
	protected $positionStart;
	/** @var int */
	protected $positionEnd;
	/** @var mixed */
	protected $default;
	/** @var  MaskEnum */
	protected $mask;
	/** @var  string */
	protected $pattern;
	/** @var  string */
	protected $key;

	//<editor-fold desc="Getters and Setters">
	/**
	 * @return string
	 */
	public function getDescription() {
		return $this->description;
	}

	/**
	 * @param string $description
	 * @return $this
	 */
	public function setDescription( $description ) {
		$this->description = $description;
		return $this;
	}

	/**
	 * @return Picture
	 */
	public function getPicture() {
		return $this->picture;
	}

	/**
	 * @param Picture $picture
	 * @return $this
	 */
	public function setPicture( $picture ) {
		$this->picture = $picture;
		return $this;
	}

	/**
	 * @return int
	 */
	public function getPositionStart() {
		return $this->positionStart;
	}

	/**
	 * @param int $positionStart
	 * @return $this
	 */
	public function setPositionStart( $positionStart ) {
		$this->positionStart = $positionStart;
		return $this;
	}

	/**
	 * @return int
	 */
	public function getPositionEnd() {
		return $this->positionEnd;
	}

	/**
	 * @param int $positionEnd
	 * @return $this
	 */
	public function setPositionEnd( $positionEnd ) {
		$this->positionEnd = $positionEnd;
		return $this;
	}


	/**
	 * Calcula o tamanho entre a posicao inicial e final
	 *
	 * @return int
	 */
	public function getPosicaoLength() {
		return $this->positionEnd - ( $this->positionStart - 1 );
	}

	/**
	 * @param mixed $default
	 * @return $this
	 */
	public function setDefault( $default ) {
		$this->default = $default;
		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getDefault() {
		return $this->default;
	}

	/**
	 * @return MaskEnum
	 */
	public function getMask() {
		return $this->mask;
	}

	/**
	 * @param MaskEnum $mask
	 * @return $this
	 */
	public function setMask( $mask ) {
		$this->mask = $mask;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getPattern() {
		return $this->pattern;
	}

	/**
	 * @param string $pattern
	 * @return $this
	 */
	public function setPattern( $pattern ) {
		$this->pattern = $pattern;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getKey() {
		return $this->key;
	}

	/**
	 * @param string $key
	 * @return $this
	 */
	public function setKey( $key ) {
		$this->key = $key;
		return $this;
	}
	//</editor-fold>

	/**
	 * preencher dados
	 *
	 * @param array $arr
	 * @return $this
	 * @throws InvalidTemplateException
	 */
	public function fill( array $arr ) {
		$this->description = $arr[ 'description' ];
		$this->picture = new Picture( $arr[ 'picture' ] );
		$this->positionStart = $arr[ 'pos' ][ 0 ];
		$this->positionEnd = $arr[ 'pos' ][ 1 ];
		$this->default = isset( $arr[ 'default' ] ) ? $arr[ 'default' ] : null;

		$mask = isset( $arr[ 'mask' ] ) ? $arr[ 'mask' ] : null;
		if ( $mask != null ) {
			$this->mask = new MaskEnum( $mask );
			$this->pattern = isset( $arr[ 'pattern' ] ) ? $arr[ 'pattern' ] : null;
		}
		return $this;
	}

//	/**
//	 * Ajusta o formato de data e hora para o padrao do php
//	 *
//	 * @param MaskEnum $mask
//	 * @param               $format
//	 * @return mixed|null
//	 */
//	public function replaceFormatDateTime( MaskEnum $mask, $format ) {
//		switch ( $mask->getValue() ) {
//			case MaskEnum::Date:
//				return str_replace(
//					array( 'DD', 'MM', 'AAAA', 'AA' ),
//					array( 'd', 'm', 'Y', 'y' ),
//					$format
//				);
//			case MaskEnum::Time:
//				return str_replace(
//					array( 'HH', 'MM', "SS", ),
//					array( 'H', 'i', 's' ),
//					$format
//				);
//		}
//		return null;
//	}

	/**
	 * @param $text
	 * @return mixed|string
	 * @throws InvalidTemplateException
	 */
	public function decode( $text ) {
		if ( $text == null )
			$text = $this->default;

		$this->resolveMask();

		$picture = $this->getPicture();
		$maskEnum = $this->getMask()->getValue();

		switch ( $maskEnum ) {
			case MaskEnum::Float:
			case MaskEnum::Int:
				$text = trim( $text );
				if ( !is_numeric( $text ) )
					throw new InvalidTemplateException( "O formato solicitado '" . $picture->getFormat() . "', precisa que seu valor '$text' seja do tipo numérico" );

				if ( $maskEnum == MaskEnum::Float ) {
					list( $value1 ) = $picture->getFormatValues();
					return (double)substr_replace( $text, '.', $value1, 0 );
				}
				if ( $text < PHP_INT_MAX )
					return (int)$text;
				return ltrim( $text, '0' );
			case MaskEnum::String:
				return trim( $text );//rtrim
			case MaskEnum::Date:
				$mask = $this->getPattern();
				return Carbon::createFromFormat( $mask, $text );
			case MaskEnum::Time:
				$mask = $this->getPattern();
				return Carbon::createFromFormat( $mask, $text );
			default:
				return $text;
		}
	}

	/**
	 * @param $value
	 * @return int|mixed|null|string
	 * @throws InvalidTemplateException
	 */
	public function encode( $value ) {
		$default = $this->getDefault();
		$picture = $this->getPicture();
		$this->resolveMask();
		$mask = $this->getMask();
		$maskValue = $mask->getValue();

		if ( $value == null ) {
			if ( $default == null ) {
				switch ( $maskValue ) {
					case MaskEnum::Int:
					case MaskEnum::Float:
						$default = 0;
						break;
					case MaskEnum::String:
						$default = '';
						break;
					case MaskEnum::Date:
					case MaskEnum::DateTime:
					case MaskEnum::Time:
						$default = null;
						break;
				}
			}
			$value = $default;
		}

		if ( $maskValue == MaskEnum::Float || $maskValue == MaskEnum::Int ) {
			if ( !is_numeric( $value ) )
				throw new InvalidTemplateException( "O formato solicitado '" . $picture->getFormat() . "', precisa que seu valor '$value' seja do tipo numérico" );

			if ( $maskValue == MaskEnum::Float ) {
				list( $value1, $value2 ) = $picture->getFormatValues();
				if ( $value2 <= 0 )
					throw new InvalidTemplateException( 'O numero de casas decimais após a virgula deve ser 1 ou maior' );

				$multiplicado = pow( 10, $value2 );
				$semArredondamento = intval( strval( $value * $multiplicado ) ) / $multiplicado;
				$valueStr = number_format( $semArredondamento, $value2, '', '' );

				$picture->validValue( $valueStr );
				return \str_pad( $valueStr, ( $value1 + $value2 ), '0', STR_PAD_LEFT );
			} else {
				$picture->validValue( $value );
				list( $value1 ) = $picture->getFormatValues();
				return \str_pad( $value, $value1, '0', STR_PAD_LEFT );
			}
		} else if ( $maskValue == MaskEnum::Date || $maskValue == MaskEnum::Time || $maskValue == MaskEnum::DateTime ) {
			$pattern = $this->getPattern();
//			$pattern = $this->replaceFormatDateTime( $mask, $pattern );
			if ( $value instanceof \DateTime ) {
				$value = $value->format( $pattern );
			}
			$picture->validValue( $value );
			return $value;

		} else if ( $maskValue == MaskEnum::String ) {
			$picture->validValue( $value );
			list( $value1 ) = $picture->getFormatValues();
			return \str_pad( $value, (int)$value1, ' ', STR_PAD_RIGHT );
		}
		return null;
	}

	/**
	 *
	 */
	private function resolveMask() {
		$picture = $this->getPicture();

		if ( $this->getMask() == null ) {
			//detect automatic (alfanumerico/numerico)
			$tipo = $picture->getFormatTipo();
			if ( $tipo == PictureEnum::Numerico ) {
				if ( $picture->isFormatFloat() )
					$this->setMask( new MaskEnum( MaskEnum::Float ) );
				else
					$this->setMask( new MaskEnum( MaskEnum::Int ) );
			} else
				$this->setMask( new MaskEnum( MaskEnum::String ) );
		}
	}

}